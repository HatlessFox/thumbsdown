//
//  PhysicsManager.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "PhysicsManager.h"
#import "SimObjFactory.h"
#import "CollisionResolver.h"
#import "CollisionDetectionEngine.h"
#import "GameManager.h"

@interface PhysicsManager ()
@property (assign, nonatomic) CFTimeInterval timeDelta;
@end

@implementation PhysicsManager {
    NSMutableSet *_alwaysMovingObjects;
    NSMutableSet *_firmObjects;
    NSMutableDictionary *_physObjectsRegistry;
    
    CFTimeInterval _lastTime;
    CollisionDetectionEngine *_cdEngine;
}

-(CFTimeInterval)lastTime { return _lastTime; }
-(void)setLastTime:(CFTimeInterval)newVal { _lastTime = newVal; }

-(NSMutableSet *)alwaysMovingObjects { return _alwaysMovingObjects; }
-(NSMutableSet *)firmObjects { return _firmObjects; }
-(CollisionDetectionEngine *)cdEngine { return _cdEngine; }
-(NSMutableDictionary *)physObjRegistry { return _physObjectsRegistry; }

-(id)init {
    self = [super init];
    if (self) {
        _physObjectsRegistry = [NSMutableDictionary new];
        _alwaysMovingObjects = [NSMutableSet new];
        _firmObjects = [NSMutableSet new];
        _cdEngine = [CollisionDetectionEngine new];
        _lastTime = CACurrentMediaTime();
    }
    return self;
}

+(GLKVector3)gravityAcceleration {
    static GLKVector3 gravity = {0, -0.2, 0};
    return gravity;
}

+(PhysicsManager *)instance {
    static PhysicsManager *inst;
    if (!inst) {
        inst = [PhysicsManager new];
    }
    return inst;
}

+(void)addAlwaysMovingObject:(NSString *)objName {
    PhysicsManager *pm = [PhysicsManager instance];
    [[pm alwaysMovingObjects] addObject:objName];
}

+(void)addFirmObject:(SimulationObject *)obj {
    PhysicsManager *pm = [PhysicsManager instance];
    [[pm firmObjects] addObject:obj.name];
    [pm physObjRegistry][obj.name] = obj.physicsSt.physObject;
    [[pm cdEngine] addObjectById:obj.name];
}

+(void)forgetObject:(NSString *)objName {
    PhysicsManager *pm = [PhysicsManager instance];
    [[pm alwaysMovingObjects] removeObject:objName];
    [[pm physObjRegistry] removeObjectForKey:objName];
    [[pm firmObjects] removeObject:objName];
    [[pm cdEngine] removeObjectById:objName];
}

-(void)tryUpdatePosition:(SimulationObject *)obj withMvmnt:(GLKVector3)mvmnt
            discardArray:(NSMutableArray *)objToRemove {
    
    if (mvmnt.x == 0 && mvmnt.y == 0 && mvmnt.z == 0) { return; }
    
    [obj.physicsSt updateTmpObjectPositionWithMvmnt:mvmnt];
    
    if (obj.physicsSt.ttl <= 0) {
        
        [objToRemove addObject:obj.name];
        return;
    }
    return;
}

-(void)updatePositionForObjectId:(NSString *)objId
                     timeElapsed:(CFTimeInterval)timeDelta
         discardArray:(NSMutableArray *)objToRemove{
    SimulationObject *obj = [SimObjFactory createdObjectById:objId];
    
    [obj.physicsSt updateVelocity:timeDelta];
    [self tryUpdatePosition:obj withMvmnt:obj.physicsSt.currVelocity
               discardArray:objToRemove];
}

+(void)updatePhysics {
    PhysicsManager *pm = [PhysicsManager instance];
    CFTimeInterval currentTime = CACurrentMediaTime();
    pm.timeDelta = (currentTime - pm.lastTime) / [GameManager slowDownFactor];
    pm.lastTime = currentTime;
    
    NSMutableArray *objToRemove = [NSMutableArray new];

    for (NSString* objId in [pm alwaysMovingObjects]){
        [pm updatePositionForObjectId:objId timeElapsed:pm.timeDelta discardArray:objToRemove];
    }
    [SimObjFactory removeObjects:objToRemove];
    [pm handleCollisions];
}

+(SimulationObject *)lookupFirmObjectByLocation:(GLKVector4)pos {
    for (NSString *firmObjId in [[self instance] firmObjects]) {
        SimulationObject *firmObj = [SimObjFactory createdObjectById:firmObjId];
        if ([firmObj.physicsSt.physObject checkIfPosInside:pos]) { return firmObj;}
    }
    return nil;
}

+(PhysicsObject *)physicsObjectById:(NSString *)objName {
    PhysicsManager *pm = [PhysicsManager instance];
    return [pm physObjRegistry][objName];
}

#pragma mark - Collision Detection

+(BOOL)checkCollisionsForObject:(NSString *)objName {
    return [[[PhysicsManager instance] cdEngine] doesObjectCollidesWithSmth:objName];
}

-(void)handleCollisions {
    //handle collisions
    [CollisionResolver prepareForCheck];
    
    [_cdEngine processMovements];
    //[_cdEngine resolveCollisions:[_firmObjects allObjects]];
    [SimObjFactory removeObjects:[CollisionResolver discardedObjectsIds]];
   
    //commit movements
    for (NSString* objId in [self alwaysMovingObjects]){
        SimulationObject *obj = [SimObjFactory createdObjectById:objId];
        [obj commitWorldTransform];
    }
}

+(float)timeDelta { return [[PhysicsManager instance] timeDelta]; }

@end
