//
//  TextureDrawEffect.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "TextureDrawEffect.h"
#import "EffectUtils.h"
#import "WorldCamera.h"

@implementation TextureDrawEffect {
    NSDictionary *_attributes;
    OGLKTextureInfo *_textureInfo;
}

- (id)init {
    NSDictionary *tmpAttributes = @{@"a_position":@1, @"a_textureCoord":@2};
    self = [super initWithShader:@"SingleTexture" andAttributes:tmpAttributes];
    if (self) {
        _attributes = tmpAttributes;
        _rowShift = 0;
    }
    return self;
}

-(instancetype)initWithImageName:(NSString *)imageName {
    self = [self init];
    if (self) {
        CGImageRef imgRef = [[UIImage imageNamed:imageName] CGImage];
        _textureInfo = [EffectUtils textureWithCGImage:imgRef options:nil];
    }
    return self;
}


- (void)bindAttributes:(id<OGLBuffer>)buffer {
    [buffer makeActive];
    [buffer enableAttributeSupport:1
                            ofType:GL_FLOAT withComponentsCount:3
                          byOffset:0];
    [buffer enableAttributeSupport:2
                            ofType:GL_FLOAT withComponentsCount:2 byOffset:3*sizeof(float)];
}

- (void)configureUniforms:(GLKMatrix4)meshTransform {
    static GLint viewProjMatrUniform[3] = {-1, -1, -1};
    if (viewProjMatrUniform[0] == -1) {
        [self extractUniformLocations:@[@"u_viewProjectionMatrix", @"u_sampler", @"u_rowShift"]
                            toStorage:viewProjMatrUniform];
    }
    
    GLKMatrix4 mvpM = meshTransform;
    WorldCamera *cam = [WorldCamera instance];
    mvpM = GLKMatrix4Multiply(cam.viewMatrix, mvpM);
    mvpM = GLKMatrix4Multiply(cam.projectionMatrix, mvpM);
    
    glUniformMatrix4fv(viewProjMatrUniform[0], 1, 0, mvpM.m);
    glUniform1i(viewProjMatrUniform[1], 0);
    glUniform1f(viewProjMatrUniform[2], _rowShift);
}


- (void)prepareToDraw {
    [super prepareToDraw];
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _textureInfo.bufferId);
}

@end
