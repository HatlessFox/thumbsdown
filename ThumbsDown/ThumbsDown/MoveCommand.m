//
//  WarriorMoveCommand.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "MoveCommand.h"
#import "PhysicsManager.h"
#import "SimObjFactory.h"

@implementation MoveCommand {
    GLKVector3 _translation;
    NSString * _actorName;
}

-(id)initWithActorName:(NSString *)actorName
           translation:(GLKVector3)translation {
    self = [super init];
    if (self) {
        _actorName = actorName;
        _translation = translation;
    }
    return self;
}

-(BOOL)performCommand {
    SimulationObject *obj = [SimObjFactory createdObjectById:_actorName];
    if (!obj) { return NO; }
    
    obj.desiredWorldTransformMtx = obj.worldTransformMtx;
    [obj.physicsSt updateTmpObjectPositionWithMvmnt:_translation];

    BOOL canBeMoved = ![PhysicsManager checkCollisionsForObject:_actorName];
    if (canBeMoved) {
        [obj commitWorldTransform];
    }
    return canBeMoved;
}

@end
