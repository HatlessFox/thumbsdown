//
//  RectangleCollisionBox.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/27/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "RectangleCollisionBox.h"

@implementation RectangleCollisionBox {
    float _width;
    float _heigth;
}


//designated
-(id)initWithWidth:(float)width height:(float)height andTransposition:(GLKMatrix4)selfTransposition {
    self = [super initWithTranslation:selfTransposition];
    if (self) {
        _width = width;
        _heigth = height;
    }
    return self;
}


-(id)initWithWidth:(float)width andHeight:(float)height {
    return [self initWithWidth:width height:height andTransposition:GLKMatrix4Identity];
}

-(id)initWithBounds:(CGRect)bounds {
    GLKMatrix4 selfTransposition = GLKMatrix4MakeTranslation(bounds.origin.x, bounds.origin.y, 0.0f);
    
    return [self initWithWidth:bounds.size.width
                        height:bounds.size.height
              andTransposition:selfTransposition];
}


#pragma mark - Collisions

//assume start <= end
+(BOOL)isOverlapsP1S:(float)start1 andP1E:(float)end1
             withP2S:(float)start2 andP2E:(float)end2 {
    return !(end2 <= start1 || end1 <= start2);
}

-(BOOL)doesCollideWithRectBox:(RectangleCollisionBox *)box {
    CGRect intRect = CGRectIntersection(_bounds, box.bounds);
    return intRect.size.height > FLT_EPSILON && intRect.size.width > FLT_EPSILON;
}

+(GLKVector4)boxOriginWorldPosition:(RectangleCollisionBox *)box {
    return GLKMatrix4MultiplyVector4(box.boxTransposition, RCB_zeroVector4);
}


-(void)updateBoxTransposition {
    GLKVector4 origin = [RectangleCollisionBox boxOriginWorldPosition:self];
    _bounds = CGRectMake(origin.x, origin.y, _width, _heigth);
}

-(BOOL)checkIfPosInside:(GLKVector4)pos {
    return
        self.minX <= pos.x && pos.x <= self.maxX &&
        self.minY <= pos.y && pos.y <= self.maxY;
}

-(CGRect)roughBoundingRect { return self.bounds; }

@end
