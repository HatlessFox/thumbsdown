//
//  AnimationManager.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/23/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "AnimationManager.h"
#import "SimulationInfo.h"
#import "SkeletonKeyFrame.h"
#import "HumanSkeletonConstants.h"

@interface AnimationManager ()

@property NSMutableDictionary *runningAnimations;

@end

@implementation AnimationManager {
    NSMutableDictionary *_animations;
}

-(void)setUpAnimations {
    SkeletonKeyFrame *frame1, *frame2, *frame3;
    
    frame1 = [[SkeletonKeyFrame alloc] initWithDuration:0.1 jointRotation:@{hsRightAnkleId: @(M_PI / 6), hsLeftAnkleId: @(M_PI / 6)} transposition:GLKVector3Make(0, 0, 0)];
    frame2 = [[SkeletonKeyFrame alloc] initWithDuration:0.1 jointRotation:[SimObjFactory warriorInitLegJointsCfg] transposition:GLKVector3Make(0, 0.0, 0)];
    
    _animations[@"WarriorJumping"] = [[Animation alloc] initWithKeyFrames:@[frame1, frame2]];
    
    frame1 = [[SkeletonKeyFrame alloc] initWithDuration:0.1 jointRotation:@{hsLeftHipId: @(-M_PI * 3 / 5), hsRightHipId:@(-M_PI * 3 / 5), hsLeftKneeId: @(M_PI/10), hsRightKneeId: @(M_PI/10)} transposition:GLKVector3Make(0, 0.05, 0)];
    frame2 = [[SkeletonKeyFrame alloc] initWithDuration:0.8
            jointRotation:@{} transposition:GLKVector3Make(0, -0.2, 0)];
    frame3 = [[SkeletonKeyFrame alloc] initWithDuration:0.3 jointRotation:[SimObjFactory warriorInitLegJointsCfg] transposition:GLKVector3Make(0, 0.85, 0)];
    _animations[@"WarriorLanding"] = [[Animation alloc] initWithKeyFrames:@[frame1, frame2, frame3]];
	
	frame1 = [[SkeletonKeyFrame alloc] initWithDuration:0.1 jointRotation:@{hsLeftKneeId: @(M_PI_4 * 3), hsRightKneeId: @(M_PI_4 * 3)} transposition:GLKVector3Make(0, 0.0, 0)];
    _animations[@"WarriorDeath"] = [[Animation alloc] initWithKeyFrames:@[frame1]];
}

+(void)registerAnimations:(NSDictionary *)anims {
    NSMutableDictionary *animations = [[AnimationManager instance] animations];
    for (NSString *animKey in anims) {
        animations[animKey] = anims[animKey];
    }
}

-(id)init {
    self = [super init];
    if (self) {
        _runningAnimations = [NSMutableDictionary new];
        
        _animations = [NSMutableDictionary new];
        [self setUpAnimations];
    }
    return self;
}

-(NSMutableDictionary *)animations { return _animations; }


+(void)addAnimation:(Animation *)anim byKey:(NSString *)key
          withMerge:(BOOL)mergeEnabled{
    
    AnimationManager *am = [self instance];

    Animation *storedAnimation = am.runningAnimations[key];
    if (storedAnimation) {
        if (mergeEnabled) { [storedAnimation merge:anim]; }
    } else {
        am.runningAnimations[key] = anim;
    }
}

+(void)addAnimationId:(NSString *)animId byKey:(NSString *)key forTarget:(NSString *)targetId {
    AnimationManager *am = [self instance];
    Animation *anim = [[am animations][animId] createCopy];
    if (!anim) { return; }
    [anim setTargetId:targetId];
    
    Animation *storedAnimation = am.runningAnimations[key];
    if ([targetId isEqualToString:storedAnimation.targetId]) {
        [storedAnimation merge:anim];
    } else {
        am.runningAnimations[key] = anim;
    }
}


+(AnimationManager *)instance {
    static AnimationManager *instance;
    if (!instance) {
        instance = [AnimationManager new];
    }
    return instance;
}


+(void)perfromAnimations:(float)elapsedTime {
    AnimationManager *am = [self instance];
    NSMutableDictionary *animations = [am.runningAnimations copy];
    
    for (NSString *objId in animations) {
        BOOL animIsFinished = [animations[objId] step:elapsedTime];
        if (animIsFinished) {
            [am.runningAnimations removeObjectForKey:objId];
        }
    }
}


@end
