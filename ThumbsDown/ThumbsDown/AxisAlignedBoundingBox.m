//
//  RectangleCollisionBox.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "AxisAlignedBoundingBox.h"
#import "CompositeCollisionBox.h"
#import "ObjectOrientedBoundingBox.h"

@implementation AxisAlignedBoundingBox

-(BOOL)checkCollision:(CollisionBox *)actor {
    //TODO refactor
    if ([actor isMemberOfClass:[CompositeCollisionBox class]]) {
        return [actor checkCollision:self];
    }

    if ([actor isMemberOfClass:[ObjectOrientedBoundingBox class]]) {
        return [actor checkCollision:self];
    }
    
    if (![actor isKindOfClass:[RectangleCollisionBox class]]) {
        NSLog(@"Can't check collision");
        return NO;
    }

    return [self doesCollideWithRectBox:(RectangleCollisionBox *)actor];
}


-(float)maxX {
    CGRect bnds = self.bounds;
    return bnds.origin.x + bnds.size.width;
}

-(float)minX { return self.bounds.origin.x; }

-(float)maxY {
    CGRect bnds = self.bounds;
    return bnds.origin.y + bnds.size.height;
}

-(float)minY { return self.bounds.origin.y; }


@end
