//
//  Knife.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/5/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Knife.h"
#import "SimObjFactory.h"
#import "BonePhysicsObject.h"
#import "HumanSkeletonConstants.h"
#import "Animation.h"
#import "SkeletonKeyFrame.h"
#import "AnimationManager.h"
//#import "SimulationInfo.h"
#import "InputManager.h"
#import "WarriorGameStrategy.h"
#import "PhysicsManager.h"

@implementation Knife {
    NSString *_parentWarriorId;
}


-(float)fireRate { return 1; }

-(NSArray *)createBulletWithId:(NSString *)bId; {
    InputManager *im = [InputManager instance];
    if (im.aimOn) {
        //* throw knife
        //remove knife from skeleton
        SimulationObject *warrior = [SimObjFactory createdObjectById:_parentWarriorId];
        SkeletonPhysicsObject *spo = (SkeletonPhysicsObject *)warrior.physicsSt.physObject;
        Joint *rightWristJoint = [spo jointById:hsRightWristId];
        [spo removeJoint:rightWristJoint.name];
        [spo removePart:@"Weapon"];
        [((WGS *)warrior.gameSt) addWeapon:nil];
        
        
        SimulationObject *knife = [SimObjFactory createObjectOfType:@"knife"
                                                              withId:bId];
        knife.physicsSt.currVelocity = GLKVector3Make(-0.05, 0.2, 0);
        knife.physicsSt.currAcceleration = GLKVector3Make(-0.005, -0.05, 0);
        knife.physicsSt.ttl = 1000;
        knife.physicsSt.physObject.ignoredCollisionsWithObjs =
                [[NSMutableSet alloc] initWithArray:@[_parentWarriorId]];
        ((Knife *)knife.gameSt).thrown = YES;
        
        return @[knife];
    } else {
        [AnimationManager addAnimationId:@"WarriorFire" byKey:@"WarriorFire" forTarget:_parentWarriorId];
        return nil;
    }
}

-(void)assignToWarrior:(SkeletonPhysicsObject *) sltn {
    _parentWarriorId = sltn.name;
    BonePhysicsObject *wbpo = (BonePhysicsObject *)[SimObjFactory createdObjectById:self.name].physicsSt.physObject;
    BonePhysicsObject *rArmBpo = (BonePhysicsObject *)[sltn physicsPartByName:@"RightArmLo"];
    
    wbpo.ignoredCollisionsWithObjs = [[NSMutableSet alloc] initWithArray:@[sltn.name]];
    
    Joint *rightWristJoint = [[Joint alloc] initWithName:hsRightWristId
                                              parentBone:rArmBpo.bone childBone:wbpo.bone
                                               connPoint:GLKVector3Make(-0.02, -0.15, 0.0)];
    
    [sltn addJoint:rightWristJoint];
    [rightWristJoint rotateChildOverJoint:-M_PI*2/5];
    //[rightWristJoint setRotationConstrMinAng:-M_PI_2-M_PI/30  maxRotAngle:-M_PI_2 + M_PI/30 isFwrd:YES];
    [sltn addPart:wbpo];
    
}

-(NSDictionary *)weaponRelatedAnimations {
    return @{
             @"WarriorAimOn":[[Animation alloc] initWithKeyFrames:
                              @[[[SkeletonKeyFrame alloc] initWithDuration:0.2 jointRotation:
                                 @{hsRightShoulderId: @(-M_PI_2), hsRightElbowId: @(M_PI),
                                   }
                                                             transposition:GLKVector3Make(0, 0.0, 0)]]],
             @"WarriorAimOff":[[Animation alloc] initWithKeyFrames:
                               @[[[SkeletonKeyFrame alloc] initWithDuration:0.2 jointRotation:
                                  @{hsRightShoulderId: @(M_PI/7), hsRightElbowId: @(M_PI/3),
                                    hsLeftShoulderId:@(-M_PI*1/5), hsLeftElbowId:@(-M_PI_2)}
                                                              transposition:GLKVector3Make(0, 0.0, 0)]]],
             @"WarriorFire":[[Animation alloc] initWithKeyFrames:
                @[[[SkeletonKeyFrame alloc] initWithDuration:0.2 jointRotation:
                    @{hsRightShoulderId: @(-M_PI*2/5), hsRightElbowId: @(M_PI), hsRightWristId:@(-M_PI_4)}
                    transposition:GLKVector3Make(0, 0.0, 0)
                   ],
                  [[SkeletonKeyFrame alloc] initWithDuration:0.4 jointRotation:
                   @{hsRightShoulderId: @(M_PI/7), hsRightElbowId: @(M_PI/3),
                     hsLeftShoulderId:@(-M_PI*1/5), hsLeftElbowId:@(-M_PI_2)}
                    transposition:GLKVector3Make(0, 0.0, 0)
                  ]
                ]]
             };
}

-(void)postprocessShoot:(GLKVector3)shootDirection {}

@end
