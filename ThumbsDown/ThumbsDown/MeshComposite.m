//
//  MeshComposite.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/18/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "MeshComposite.h"


@implementation MeshComposite {
    NSArray *_meshes;
}

//deny plain mesh comstructor
-(id)initWithBuffer:(OGLServerBuffer *)buffer
   numberOfElements:(GLuint)elemsCnt
         StartingFrom:(GLuint)offset
             WithEffect:(OGLEffect *)effect
           drawMode:(GLenum)mode {
    NSAssert(false, @"Standard mesh constructor is denied for composide mesh");
    return nil;
}

-(id)initWithMeshes:(NSArray *)meshes {
    self = [super init];
    if (self) {
        _meshes = meshes;
    }
    return self;
}

-(void)draw:(GLKMatrix4)meshTransform {
    for (Mesh *mesh in _meshes) {
        [mesh draw:GLKMatrix4Multiply(meshTransform, [self currentTranslation])];
    }
}

-(GLKMatrix4)currentTranslation {
    return self.transpositionProvider ? [self.transpositionProvider currentTransposition] :
        GLKMatrix4Identity;
}

@end
