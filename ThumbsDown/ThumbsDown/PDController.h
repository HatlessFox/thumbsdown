//
//  PDController.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/6/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GLKit;

@interface PDController : NSObject

@property (assign, nonatomic) GLKVector3 desiredValue;
@property (assign, nonatomic) GLKVector3 currentValue;

-(id)initWithCurrentPosition:(GLKVector3)pos
                      daming:(float)damping
                 springConst:(float)springConst;

-(void)update:(float)timeDelta;


@end
