//
//  AK.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/4/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "AK.h"
#import "SimObjFactory.h"
#import "BonePhysicsObject.h"
#import "HumanSkeletonConstants.h"
#import "Animation.h"
#import "SkeletonKeyFrame.h"
#import "WorldCamera.h"

@implementation AK

-(float)fireRate { return 0.15; }

-(NSArray *)createBulletWithId:(NSString *)bId; {
    SimulationObject *bullet = [SimObjFactory createObjectOfType:@"bullet"
                                                          withId:bId];
    bullet.physicsSt.currVelocity = GLKVector3Make(-0.3, 0.0, 0);
    bullet.physicsSt.ttl = 1000;
    
    //FIXME: pull back
    [WorldCamera pullback:GLKVector3Make(-0.5, 0, 0)];
    
    return @[bullet];
}

-(void)assignToWarrior:(SkeletonPhysicsObject *)sltn {
    BonePhysicsObject *wbpo = (BonePhysicsObject *)[SimObjFactory createdObjectById:self.name].physicsSt.physObject;
    BonePhysicsObject *rArmBpo = (BonePhysicsObject *)[sltn physicsPartByName:@"RightArmLo"];
    
    Joint *rightWristJoint = [[Joint alloc] initWithName:hsRightWristId
                                              parentBone:rArmBpo.bone childBone:wbpo.bone
                                               connPoint:GLKVector3Make(0.10, 0.0, 0.0)];
    
    [sltn addJoint:rightWristJoint];
    [rArmBpo.bone.childJoints addObject:rightWristJoint];
    [rightWristJoint rotateChildOverJoint:-M_PI_2];
    [rightWristJoint setRotationConstrMinAng:-M_PI_2-M_PI/30  maxRotAngle:-M_PI_2 + M_PI/30 isFwrd:YES];
    [sltn addPart:wbpo];
    
}


-(NSDictionary *)weaponRelatedAnimations {
    return @{
             @"WarriorAimOn":[[Animation alloc] initWithKeyFrames:@[[[SkeletonKeyFrame alloc] initWithDuration:0.2 jointRotation:@{hsRightShoulderId: @(M_PI * 3/8), hsRightElbowId: @(M_PI/8), hsRightWristId:@(-M_PI_2), hsLeftShoulderId:@(-M_PI/10), hsLeftElbowId:@(-M_PI*6/8)} transposition:GLKVector3Make(0, 0.0, 0)]]],
             @"WarriorAimOff":[[Animation alloc] initWithKeyFrames:@[[[SkeletonKeyFrame alloc] initWithDuration:0.2 jointRotation:@{hsRightShoulderId: @(0), hsRightElbowId: @(M_PI_2), hsLeftShoulderId:@(-M_PI/5), hsLeftElbowId:@(-M_PI/5)} transposition:GLKVector3Make(0, 0.0, 0)]]],
             
             @"WeaponRecoil":[[Animation alloc] initWithKeyFrames:
                              @[[[SkeletonKeyFrame alloc] initWithDuration:0.05 jointRotation:@{
                        hsRightElbowId: @(M_PI_2-M_PI/6 * rand() / RAND_MAX),
                        hsLeftElbowId:@(-M_PI/5-M_PI/6 * rand() / RAND_MAX)} transposition:GLKVector3Make(0, 0.0, 0)],
                      [[SkeletonKeyFrame alloc] initWithDuration:0.1 jointRotation:@{hsRightElbowId: @(M_PI_2),hsLeftElbowId:@(-M_PI/5)} transposition:GLKVector3Make(0, 0.0, 0)]
                                 
                     ]
            ],
            @"WeaponRecoilAiming":[Animation new]
             
             };
}

-(void)postprocessShoot:(GLKVector3)shootDirection {
    [WorldCamera pullback:shootDirection];
}



@end
