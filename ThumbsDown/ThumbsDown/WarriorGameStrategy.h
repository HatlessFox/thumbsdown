//
//  WarriorGameStrategy.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/4/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Weapon.h"

@interface WarriorGameStrategy : GameStrategy

@property (assign, nonatomic) NSString *weaponId;
@property (readonly, nonatomic) id<Weapon> currWeapon;
@property (nonatomic, assign) int hitPoints;

-(void)addWeapon:(NSString *)wpnType;

@end

typedef WarriorGameStrategy WGS;