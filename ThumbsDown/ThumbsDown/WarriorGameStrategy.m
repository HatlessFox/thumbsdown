//
//  WarriorGameStrategy.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/4/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "WarriorGameStrategy.h"
#import "SimObjFactory.h"
#import "AnimationManager.h"

@implementation WarriorGameStrategy {
    __weak SimulationObject *_cachedWpn;
}

-(id<Weapon>)currWeapon { return (id<Weapon>)_cachedWpn.gameSt; }

-(void)setWeaponId:(NSString *)weaponId {
    if (_weaponId) {
        [SimObjFactory removeObject:_weaponId];
    }
    
    _weaponId = weaponId;
    _cachedWpn = [SimObjFactory createdObjectById:weaponId];
    if (!_weaponId) { return; }
    
    SimulationObject *warrior = [SimObjFactory createdObjectById:self.name];
    
    id<Weapon> newWpn = (id<Weapon>)_cachedWpn.gameSt;
    [newWpn assignToWarrior:(SkeletonPhysicsObject *)warrior.physicsSt.physObject];
    [AnimationManager registerAnimations:[newWpn weaponRelatedAnimations]];
}

-(void) setHitPoints:(int)hitPoints {
	if (_hitPoints < 0) { return; }
	
	_hitPoints = hitPoints;
	if (_hitPoints <= 0) {
		[AnimationManager addAnimationId:@"WarriorDeath" byKey:self.name forTarget:self.name];
		//FIXME: add post-handler to animation instead of this hack
		[SimObjFactory createdObjectById:self.name].physicsSt.ttl = 25;
	}
}


-(void)addWeapon:(NSString *)wpnType {
    if (!wpnType) {
        self.weaponId = nil;
        return;
    }
    
    SimulationObject *weapon = [SimObjFactory createObjectOfType:wpnType withId:[NSString stringWithFormat:@"%@-%@", self.name, wpnType]];
    self.weaponId = weapon.name;
    
    [AnimationManager addAnimationId:@"WarriorAimOff" byKey:@"PlayerAimimg"
                           forTarget:self.name];
}

@end
