//
//  ShaderUtils.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/16/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Foundation/Foundation.h"
#import "GLKit/GLKit.h"


@interface OGLKTextureInfo : NSObject

@property (assign, nonatomic, readonly) GLuint bufferId;
@property (assign, nonatomic, readonly) GLenum type;
@property (assign, nonatomic, readonly) GLuint width;
@property (assign, nonatomic, readonly) GLuint height;

@end

@interface EffectUtils : NSObject

// Loads shader with given name
// Returns:
//     id of created program in case of success
//     0 in case of failure
+ (GLuint)loadShader:(NSString *)shaderResourceName
      WithAttributes:(NSDictionary *)attrs;
        
+ (void)extractUniformLocations:(NSArray *)uniformNames
                    fromProgram:(GLuint)programId
                      toStorage:(GLint *)storage;

+ (GLuint)loadShader:(NSString *)shaderResourceName
      WithAttributes:(NSDictionary *)attrs
  andExtractUniforms:(NSArray *)uniformNames
           toStorage:(GLint *)storage;


+ (OGLKTextureInfo *)textureWithCGImage:(CGImageRef)cgImage
                                options:(NSDictionary *)options;

@end
