//
//  ParticleEmitter.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/4/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GLKit;

@protocol ParticleEmitter <NSObject>

@required
-(GLKVector3)particleEmitPoint;

@end
