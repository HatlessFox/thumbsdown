//
//  SimObjFactory.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimulationObject.h"

//TODO preevent singleton multiple instantiation
@interface SimObjFactory : NSObject

+(SimulationObject *)createObjectOfType:(NSString *)typeId
                                 withId:(NSString *)objId;

+(SimulationObject *)createdObjectById:(NSString *)objId;

+(void)removeObject:(NSString *)objId;
+(void)removeObjects:(id<NSFastEnumeration>)objIds;
+(NSArray *)storedObjects;

+(NSDictionary *)warriorInitLegJointsCfg;


@end
