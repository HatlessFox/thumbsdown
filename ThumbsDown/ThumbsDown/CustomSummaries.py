# LLDB script for plain summary of Axis-Aligned Bounding Box
import lldb
from operator import add as plus
from operator import mul as mult
import math

def mult_mtx_vctr(mtx, vtor):
    res = [];
    for row in mtx: res.append(reduce(plus, map(mult, row, vtor)));
    return res;

def bb_summary(val_obj, internal_dict):
    # Get box model coordinates
    bounds = val_obj.GetChildMemberWithName('_bounds');
    origin = bounds.GetChildMemberWithName('origin');
    size = bounds.GetChildMemberWithName('size');
    
    # Get box model coordinates
    err = lldb.SBError();
    originX = origin.GetChildMemberWithName('x').GetData().GetFloat(err, 0);
    originY = origin.GetChildMemberWithName('y').GetData().GetFloat(err, 0);
    modelPos = [originX, originY, 0.0, 1.0];
    
    #Get possible translation matrix
    transl_data = val_obj.GetChildMemberWithName('_translation').GetChildMemberWithName('m');
    mtx = [[], [], [], []]
    for i in range(0, 16):
        memberValue = transl_data.GetChildAtIndex(i).GetData().GetFloat(err, 0)
        mtx[i % 4].append(memberValue);
#    print(mtx)
    worldPos = mult_mtx_vctr(mtx, modelPos);
    worldStX = worldPos[0]
    worldStY = worldPos[1]
    worldEndX = worldStX + size.GetChildMemberWithName('width').GetData().GetFloat(err, 0)
    worldEndY = worldStY + size.GetChildMemberWithName('height').GetData().GetFloat(err, 0)

    # Print results
    ## (currV.x, currV.x + _bounds.size.width)
    return "from [{0:.2f},{1:.2f}] to [{2:.2f},{3:.2f}]".format(worldStX, worldStY, worldEndX, worldEndY);

def simulationobject_summary(val_obj, internal_dict):
    id = val_obj.GetChildMemberWithName('_name').GetSummary();
    type = val_obj.GetChildMemberWithName('_type').GetSummary();
    return "{0} with id {1}".format(type[2:-1], id[2:-1]);

def glkvector_summary(val_obj, internal_dict):
    return val_obj.GetChildMemberWithName('v').GetSummary();

def glkmatrix4_summary(val_obj, internal_dict):
    #extract values
    err = lldb.SBError();
    transl_data = val_obj.GetChildMemberWithName('m');
    
    isAllZero = True;
    mtx = [[], [], [], []]
    for i in range(0, 16):
        memberValue = transl_data.GetChildAtIndex(i).GetData().GetFloat(err, 0)
        mtx[i % 4].append(memberValue);
        if (memberValue): isAllZero = False;

    #get rotation angles
    if (isAllZero): return "WARNING: zero matrix";

    if math.fabs(mtx[2][0]) != 1:
        angY = math.asin(-mtx[2][0]);
        cosY = math.cos(angY);
        angX = math.atan2(mtx[2][1]/cosY, mtx[2][2]/cosY);
        angZ= math.atan2(mtx[1][0]/cosY, mtx[0][0]/cosY);
    else:
        angZ = 0;
        angY = (-1)*mtx[2][0]*math.PI / 2
        angX = math.atan2(-mtx[2][0]*mtx[0][1], -mtx[2][0]*mtx[0][2])

    return "angX={0:.1f}, angY={1:.1f}, angZ={2:.1f}; dX={3:.2f}, dY={4:.2f}, dZ={5:.2f}".format(math.degrees(angX), math.degrees(angY), math.degrees(angZ), mtx[0][3], mtx[1][3], mtx[2][3]);


# setup script
def __lldb_init_module(debugger, dict):
    debugger.HandleCommand('type summary add AxisAlignedBoundingBox --python-function CustomSummaries.bb_summary');
    debugger.HandleCommand('type summary add ObjectOrientedBoundingBox --python-function CustomSummaries.bb_summary');
    debugger.HandleCommand('type summary add SimulationObject --python-function CustomSummaries.simulationobject_summary');
    debugger.HandleCommand('type summary add GLKVector3 --python-function CustomSummaries.glkvector_summary');
    debugger.HandleCommand('type summary add GLKVector4 --python-function CustomSummaries.glkvector_summary');
    debugger.HandleCommand('type summary add GLKMatrix4 --python-function CustomSummaries.glkmatrix4_summary');