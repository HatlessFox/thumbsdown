//
//  OGLBuffer.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/16/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OGLBuffer.h"
#import "BaseOGLBuffer.h"

//Object representation on OpenGL ES buffer
@interface OGLServerBuffer: NSObject <OGLBuffer>


@property (assign, nonatomic) GLenum bufferType;
@property (assign, nonatomic) GLsizei stride;


- (void)dealloc;

@end
