//
//  DrawingCtx.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/18/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "DrawingCtx.h"

@implementation DrawingCtx

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}
+ (DrawingCtx *)instance {
    static DrawingCtx *inst;
    if (!inst) { inst = [DrawingCtx new]; }
    return inst;
}

@end
