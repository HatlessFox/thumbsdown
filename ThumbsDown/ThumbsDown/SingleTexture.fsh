//
//  SingleTexture.fsh
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

precision mediump float;

varying mediump vec2 v_textureCoord;

uniform sampler2D u_sampler;
uniform float u_rowShift;

void main() {
    vec2 textureCoord = v_textureCoord;
    textureCoord.s += floor(textureCoord.t) * u_rowShift;
    
    gl_FragColor = texture2D(u_sampler, textureCoord);
}