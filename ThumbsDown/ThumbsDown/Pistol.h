//
//  Pistol.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/4/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Weapon.h"
#import "GameStrategy.h"

@interface Pistol : GameStrategy<Weapon>

@end
