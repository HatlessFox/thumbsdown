//
//  Shotgun.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/6/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "GameStrategy.h"
#import "Weapon.h"

@interface Shotgun : GameStrategy<Weapon>

@end
