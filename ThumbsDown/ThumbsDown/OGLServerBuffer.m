//
//  OGLBuffer.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/16/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "OGLServerBuffer.h"

@implementation OGLServerBuffer {
    GLuint _bufferId;
}

#pragma mark - Constructors
//designated ctor
- (id)initWithBufferType:(GLenum)type
               andStride:(GLsizei)stride {
    self = [super init];
    if (self) {
        _bufferType = type;
        _stride = stride;
        glGenBuffers(1, &_bufferId);
        glBindBuffer(type, _bufferId);
    }
    return self;
}
- (void)reinitWithBufferType:(GLenum)type
                 andStride:(GLsizei)stride {
    _bufferType = type;
    _stride = stride;
    glBindBuffer(type, _bufferId);
}

-(void)makeActive {
    glBindBuffer(self.bufferType, _bufferId);
}

#pragma mark - Attributes
- (void)enableAttributeSupport:(GLint)attrIndex
                        ofType:(GLenum)type
           withComponentsCount:(GLint)compNum
                      byOffset:(int)offset {
    NSAssert(_bufferId != 0, @"Invalid buffer id");
    glEnableVertexAttribArray(attrIndex);
    glVertexAttribPointer(attrIndex, compNum, type, GL_FALSE,
                          _stride, (char *)offset); //hack to conform interface and save boilerplate

}

- (void)disableAttributeSupport:(GLint)attrIndex {
    glDisableVertexAttribArray(attrIndex);
}

#pragma mark - Data load & Drawing
- (void)updateDataFrom:(const GLvoid *)data
        withElemsCount:(GLsizeiptr)elemCnt {
    [self updateDataFrom:data withElemsCount:elemCnt
                                 andDrawMode: GL_DYNAMIC_DRAW];
}
- (void)updateDataFrom:(const GLvoid *)data
        withElemsCount:(GLsizeiptr)elemCnt
           andDrawMode:(GLenum)mode{
    glBindBuffer(self.bufferType, _bufferId);
    glBufferData(self.bufferType, elemCnt * self.stride, data, mode);
}

- (void)drawElemnts:(GLsizei)cnt
           withMode:(GLenum)mode
       startingFrom:(GLint)fst {
    glDrawArrays(mode, fst, cnt);
}

#pragma mark - Clean up
- (void)dealloc {
    if (_bufferId) {
        glDeleteBuffers(1, &_bufferId);
        _bufferId = 0;
    }
}

@end
