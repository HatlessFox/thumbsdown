//
//  WorldCamera.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLKit/GLKit.h"

//TODO make proper singleton
@interface WorldCamera : NSObject

@property (assign, nonatomic) CGFloat zoomFactor;

+(WorldCamera *)instance;
+(void)applyTransform:(GLKMatrix4)transform;
+(void)moveHoriz:(float)delta;
+(void)moveDepth:(float)depth
;
+(void)rotateY:(float)degrees;

+(GLKVector4)toProjView:(GLKVector4)vector;

+(void)pullback:(GLKVector3)delta;
+(void)pullback:(GLKVector3)delta withZoom:(CGFloat)zoom;

-(void)handleBoundObjTransform:(NSNotification *)notification;
+(void)bindToObject:(NSString *)objName;

+(void)update:(float)timeDelta;

@property (nonatomic, assign) GLKMatrix4 viewMatrix;
@property (nonatomic, assign) GLKMatrix4 projectionMatrix;


@end
