//
//  ViewController.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/16/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface ViewController : GLKViewController
@property (weak, nonatomic) IBOutlet UILabel *objCnt;

- (IBAction)fireBtnClick:(id)sender;
- (IBAction)jumpBtnClick:(id)sender;
- (IBAction)turnAroundClick:(id)sender;

- (IBAction)slowdownFPS:(id)sender;
- (IBAction)weaponSwitch:(id)sender;

@end
