
//
//  CompositeCollisionBox.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "CompositeCollisionBox.h"
#import "Joint.h"

@implementation CompositeCollisionBox{
    NSMutableArray *_innerParts;
    
    float _minX, _maxX, _minY, _maxY;
}

-(id)initWithChildren:(NSArray *)parts {
    self = [super init];
    if (self) {
        _innerParts = [parts mutableCopy];
    }
    return self;
}


-(BOOL)checkCollision:(CollisionBox *)actor {
    for (CollisionBox *part in _innerParts) {
        if ([part checkCollision:actor]) { return YES; }
    }
    return NO;
}

-(NSArray *)parts { return _innerParts; }

-(BOOL)checkIfPosInside:(GLKVector4)pos {
    for (CollisionBox *part in _innerParts) {
        if ([part checkIfPosInside:pos]) { return YES; }
    }
    return NO;
}


-(float)maxY {
    float result = FLT_MIN;
    for (CollisionBox *part in _innerParts) {
        result = MAX(result, [part maxY]);
    }
    return result;
}

-(float)minY {
    float result = FLT_MAX;
    for (CollisionBox *part in _innerParts) {
        result = MIN(result, [part minY]);
    }
    return result;
}


-(float)maxX {
    float result = FLT_MIN;
    for (CollisionBox *part in _innerParts) {
        result = MAX(result, [part maxX]);
    }
    return result;
}
-(float)minX {
    float result = FLT_MAX;
    for (CollisionBox *part in _innerParts) {
        result = MIN(result, [part minX]);
    }
    return result;
}

-(void)updateBoxTransposition {
    _minX = _minY = INFINITY;
    _maxX = _maxY = -INFINITY;
    for (CollisionBox *part in _innerParts) {
        [part updateBoxTransposition];
        _minX = MIN(part.minX, _minX);
        _minY = MIN(part.minY, _minY);
        _maxX = MAX(part.maxX, _maxX);
        _maxY = MAX(part.maxY, _maxY);
    }
}

@end
