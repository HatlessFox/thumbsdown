//
//  Mesh.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OGLBuffer.h"
#import "OGLEffect.h"
#import "GLKit/GLKit.h"
#import "TranslationProvider.h"

//TODO add Composite
@interface Mesh : NSObject

//TODO split to vtx and normals
@property (strong, nonatomic) NSData* meshData;
@property (strong, nonatomic) id<OGLBuffer> oglBuffer;
@property (strong, nonatomic) OGLEffect *effect;
@property (weak, nonatomic) id<TranspositionProvider> transpositionProvider;

@property (assign, nonatomic) GLuint elementsCount;
@property (assign, nonatomic) GLuint offset;

-(id)initWithBuffer:(OGLServerBuffer *)buffer
   numberOfElements:(GLuint)elemsCnt
         StartingFrom:(GLuint)offset
             WithEffect:(OGLEffect *)effect
           drawMode:(GLenum)mode;
-(void)draw:(GLKMatrix4)meshTransform;


@end
