//
//  BaseOGLBuffer.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OGLBuffer.h"

@interface BaseOGLBuffer : NSObject <OGLBuffer>

@property (assign, nonatomic) GLenum bufferType;
@property (assign, nonatomic) GLsizei stride;

@end
