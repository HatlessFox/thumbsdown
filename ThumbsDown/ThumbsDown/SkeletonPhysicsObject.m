//
//  ComposicePhysicsObject.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "SkeletonPhysicsObject.h"
#import "BonePhysicsObject.h"

@implementation SkeletonPhysicsObject {
    NSMutableDictionary *_joints;
    NSMutableDictionary *_parts;
}

-(id)initWithInnerParts:(NSMutableArray *)innerParts
       controllerObject:(PhysicsObject *)ctrlr
                 joints:(NSArray *)joints {
    self = [super init];
    if (self) {
        _parts = [NSMutableDictionary new];
        for (PhysicsObject *po in innerParts) {
            _parts[po.name] = po;
        }
        _controllerObject = ctrlr;
        _joints = [NSMutableDictionary new];
        for
            (Joint *joint in joints) {
            _joints[joint.name] = joint;
        }
    }
    return self;
}

-(Joint *)jointById:(NSString *)jointId {
    return _joints[jointId];
}

-(void)addJoint:(Joint *)joint {
    _joints[joint.name] = joint;
    [joint.parentBone.childJoints addObject:joint];
}

-(void)addPart:(PhysicsObject *)po {
    _parts[po.name] = po;
}

-(void)removeJoint:(NSString *)jointId {
    Joint *jointToRemove = _joints[jointId];
    [_joints removeObjectForKey:jointId];
    [jointToRemove.parentBone.childJoints removeObject:jointToRemove];
}

-(void)removePart:(NSString *)partId {
    [_parts removeObjectForKey:partId];
}

-(PhysicsObject *)physicsPartByName:(NSString *)name {
    return _parts[name];
}

-(void)setTransposition:(GLKMatrix4)transposition {
    [_controllerObject setTransposition:transposition];
    [self prepareForCollisionDetection];
}


-(void)prepareForCollisionDetection {
    [self.boundingBox updateBoxTransposition];
    for (PhysicsObject *part in [_parts allValues]) {
        [part.boundingBox updateBoxTransposition];
    }
}

-(NSArray *)relatedParts { return [_parts allValues]; }
-(PhysicsObject *)controllerPart { return _controllerObject; }

@end
