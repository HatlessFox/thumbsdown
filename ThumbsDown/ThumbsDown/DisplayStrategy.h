//
//  DisplayStrategy.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/18/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MeshComposite.h"

@interface DisplayStrategy : NSObject

@property (nonatomic, strong) MeshComposite* mesh;

-(id)initWithSimObjectName:(NSString *)simObjName;
-(void)draw;

@end
