//
//  ColoringOutput.fsh
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/16/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

varying mediump vec4 v_color;

void main() {
    gl_FragColor = v_color;
}