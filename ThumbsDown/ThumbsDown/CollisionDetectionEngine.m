//
//  CollisionAwareObjects.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/24/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "CollisionDetectionEngine.h"
#import "CollisionResolver.h"
#import "PhysicsManager.h"

//TODO: move to PM
#import "AnimationManager.h"

@implementation CollisionDetectionEngine {
    NSMutableArray *_tmpCollObjects;
    NSMutableArray *_collisionAwareObjectIds; //array sorted by x coordinate
    
    //objects added/removed between/in time of collision handlings
    NSMutableArray *_recentlyAddedObjects;
    NSMutableSet *_recentlyRemovedObjects;
}

-(id)init {
    self = [super init];
    if (self) {
        _collisionAwareObjectIds = [NSMutableArray new];
        _recentlyAddedObjects = [NSMutableArray new];
        _recentlyRemovedObjects = [NSMutableSet new];
    }
    return self;
}

-(void)addObjectById:(NSString *)objectId {
    [_recentlyAddedObjects addObject:objectId];
}
-(void)removeObjectById:(NSString *)objectId {
    [_recentlyRemovedObjects addObject:objectId];
}

#pragma mark - Collision Handling

-(void)sweepRemovedObjects {
    if (!_recentlyRemovedObjects.count) { return; }
    
    //TODO: implelemnt efficiently
    [_collisionAwareObjectIds removeObjectsInArray:[_recentlyRemovedObjects allObjects]];
    
    [_recentlyRemovedObjects removeAllObjects];
}

+(NSComparisonResult)compareByMaxXObject:(PhysicsObject *)obj1
                              withObject:(PhysicsObject *)obj2 {
    float obj1MaxX = [obj1.boundingBox maxX];
    float obj2MaxX = [obj2.boundingBox maxX];
    
    if (fabs(obj2MaxX - obj1MaxX) < FLT_EPSILON) { return NSOrderedSame; }
    if (obj2MaxX > obj1MaxX) { return NSOrderedAscending; }
    return NSOrderedDescending;
}

+(BOOL)isCollisionPossible:(PhysicsObject *)obj
            withPrevObject:(PhysicsObject *)prevObj {
    float objMinX = [obj.boundingBox minX];
    float objBeforeMaxX = [prevObj.boundingBox maxX];
    
    return objMinX < objBeforeMaxX;
}


-(void)prepareNewcomers{
    //sort recently added by x-max
    [_recentlyAddedObjects sortUsingComparator:^(NSString *objId1, NSString *objId2) {
        PhysicsObject *object1 = [PhysicsManager physicsObjectById:objId1];
        PhysicsObject *object2 = [PhysicsManager physicsObjectById:objId2];
        
        return [CollisionDetectionEngine compareByMaxXObject:object1 withObject:object2];
    }];
}

-(BOOL)checkCollisionsWithPreviousObjects:(int)objectIndex
                                  objects:(NSMutableArray *)objects
                         handleCollisions:(BOOL)isCollHandlingRequired{
    int arrayInd = objectIndex;
    
    NSSet *discardedTransl = [CollisionResolver discardedTranslation];
    NSString *objectId = objects[objectIndex];
    
    PhysicsObject *object = [PhysicsManager physicsObjectById:objectId];
    
    //FIXME: determine if obj static or not
    bool addedObjectIsStatic = ![[SimObjFactory createdObjectById:objectId] isMovementInProgress];
    /*
    bool addedObjectIsStatic = memcmp(object.compoundTranslation.m,
                                      object.desiredTranslation.m,
                                      sizeof(object.compoundTranslation.m)) == 0;
    */
    
    while (arrayInd > 0) {
        NSString *prevObjId = objects[arrayInd-1];
        PhysicsObject *prevObj = [PhysicsManager physicsObjectById:prevObjId];
        if ([CollisionDetectionEngine compareByMaxXObject:prevObj
                                               withObject:object] != NSOrderedDescending) {
            break;
        }
        
        if ([prevObj doesCollideWith:object]) {
            if (!isCollHandlingRequired) { return YES; }
            
            [CollisionResolver resolveCollisionOfObject:prevObjId withObject:objectId];
            if (!addedObjectIsStatic || [discardedTransl containsObject:objectId]) {
                return YES;
            }
        }
        
        [objects exchangeObjectAtIndex:arrayInd
                             withObjectAtIndex:arrayInd - 1];
        arrayInd -= 1;
    }
    
    while (arrayInd > 0) {
        NSString *prevObjId = objects[arrayInd-1];
        PhysicsObject *prevObj = [PhysicsManager physicsObjectById:prevObjId];
        
        if (![CollisionDetectionEngine isCollisionPossible:object
                                            withPrevObject:prevObj]) {
            break;
        }

        SimulationObject *soPrev = [SimObjFactory createdObjectById:prevObjId];
        if (!addedObjectIsStatic || [soPrev isMovementInProgress]) {
            if ([object doesCollideWith:prevObj]) {
                if (!isCollHandlingRequired) { return YES; }
                
                [CollisionResolver resolveCollisionOfObject:objectId withObject:prevObjId];
                if (!addedObjectIsStatic || [discardedTransl containsObject:objectId]) {
                    return YES;
                }
            }
        }
        
        
        arrayInd -= 1;
    }
    return NO;
}

-(void)processMovementsIteration {
    [self sweepRemovedObjects];
    [self prepareNewcomers];
    
    uint capacity = _collisionAwareObjectIds.count + _recentlyAddedObjects.count;
    _tmpCollObjects = [[NSMutableArray alloc] initWithCapacity:capacity];
    
    int oldObjIndex = 0;
    int newObjIndex = 0;
    //travarse objects sorted by x-max
    while (oldObjIndex < _collisionAwareObjectIds.count ||
           newObjIndex < _recentlyAddedObjects.count) {
    
        // decide current or new object want to pick
        NSString *objIdToAdd;
        if (newObjIndex == _recentlyAddedObjects.count) {
            objIdToAdd = _collisionAwareObjectIds[oldObjIndex++];
        }else if (oldObjIndex == _collisionAwareObjectIds.count) {
            objIdToAdd = _recentlyAddedObjects[newObjIndex++];
        } else {
            NSComparisonResult cmpRes = [CollisionDetectionEngine
                compareByMaxXObject:[PhysicsManager physicsObjectById:_collisionAwareObjectIds[oldObjIndex]]
                        withObject:[PhysicsManager physicsObjectById:_recentlyAddedObjects[newObjIndex]]
            ];
            switch (cmpRes) {
                case NSOrderedAscending:
                    objIdToAdd = _collisionAwareObjectIds[oldObjIndex++];
                    break;
                default:
                    objIdToAdd = _recentlyAddedObjects[newObjIndex++];
                    break;
            }
        }
      
        [_tmpCollObjects addObject:objIdToAdd];
        [self checkCollisionsWithPreviousObjects:_tmpCollObjects.count - 1
                                         objects:_tmpCollObjects
                                handleCollisions:YES];


    }
    [_recentlyAddedObjects removeAllObjects];
    _collisionAwareObjectIds = [_tmpCollObjects mutableCopy];
}

-(void)processMovements {
    NSMutableSet *discardedTranslations = [NSMutableSet new];
    while (YES) {
        NSSet *newcomers = [[NSSet alloc] initWithArray:_recentlyAddedObjects];
        

        
        [discardedTranslations addObjectsFromArray:        [[CollisionResolver discardedTranslation] allObjects]];
        [[CollisionResolver discardedTranslation] removeAllObjects];
        [self processMovementsIteration];
        
        //filter static objects
        /*
        BOOL areAllStatic = YES;
        for (NSString *discardedObjectId in [CollisionResolver discardedTranslation]) {
            if ([[SimObjFactory createdObjectById:discardedObjectId] isMovementInProgress]) {
                areAllStatic = NO;
                break;
            }
        }
        */
        if ([CollisionResolver discardedTranslation].count == 0) { break; }
        
        //remove all discarded newcomers
        if (newcomers.count > 0) {
            for (NSString *discardedObjectId in [CollisionResolver discardedTranslation]) {
                if ([newcomers containsObject:discardedObjectId]) {
                    [SimObjFactory removeObject:discardedObjectId];
                }
            }
        }
        [SimObjFactory removeObjects:[CollisionResolver discardedObjectsIds]];
    }
    //TODO: move to PhysicsManager
    for (NSString *discardedObjectId in discardedTranslations) {
        SimulationObject * so = [SimObjFactory createdObjectById:discardedObjectId];
        if ([so.type isEqualToString:@"warrior"] && so.physicsSt.currVelocity.y < -0.25) {
            [AnimationManager addAnimationId:@"WarriorLanding" byKey:@"warrior" forTarget:so.name];
        }
        
        so.physicsSt.currVelocity = GLKVector3Make(0, 0, 0);
    }
}

-(BOOL)doesObjectCollidesWithSmth:(NSString *)objId {
    unsigned objInd = [_collisionAwareObjectIds indexOfObject:objId];
    
    BOOL collDetected = [self checkCollisionsWithPreviousObjects:objInd objects:_collisionAwareObjectIds handleCollisions:FALSE];
    if (collDetected) { return TRUE; }
    
    PhysicsObject *objectToTest = [PhysicsManager physicsObjectById:objId];
    unsigned rightObjInd = objInd + 1;
    while (rightObjInd != _collisionAwareObjectIds.count) {
        NSString *rightObjId = _collisionAwareObjectIds[rightObjInd];
        PhysicsObject *rightObj = [PhysicsManager physicsObjectById:rightObjId];
        
        if (![CollisionDetectionEngine isCollisionPossible:rightObj
                                            withPrevObject:objectToTest]) {
            break;
        }
        
        if ([objectToTest doesCollideWith:rightObj]) {
            return YES;
        }
        
        rightObjInd += 1;
    }
    return NO;
}

@end
