//
//  OGLBuffer.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OGLBuffer <NSObject>

@required

-(void)makeActive;

#pragma mark - Constructors
- (id)initWithBufferType:(GLenum)type
               andStride:(GLsizei)stride;
- (void)reinitWithBufferType:(GLenum)type
                   andStride:(GLsizei)stride;

#pragma mark - Attributes
- (void)enableAttributeSupport:(GLint)attrIndex
                        ofType:(GLenum)type
           withComponentsCount:(GLint)compNum
                      byOffset:(int)offset;
- (void)disableAttributeSupport:(GLint)attrIndex;

#pragma mark - Data load & Drawing
- (void)updateDataFrom:(const GLvoid *)data
        withElemsCount:(GLsizeiptr)elemCnt;
- (void)updateDataFrom:(const GLvoid *)data
        withElemsCount:(GLsizeiptr)elemCnt
           andDrawMode:(GLenum)mode;

- (void)drawElemnts:(GLsizei)cnt
           withMode:(GLenum)mode
       startingFrom:(GLint)fst;

@optional
//NB add partial data change for client buffers if necessary


@end
