//
//  OGLEffect.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "OGLEffect.h"
#import "EffectUtils.h"

@implementation OGLEffect {
    GLuint _programId;
}

- (id)initWithShader:(NSString *)shaderName {
    return [self initWithShader:shaderName
                  andAttributes:@{}];
}

- (id)initWithShader:(NSString *)shaderName
       andAttributes:(NSDictionary *)attrs {
    self = [super init];
    if (self) {
        _programId = [EffectUtils loadShader:shaderName WithAttributes:attrs];
        if (_programId == 0) {
            NSLog(@"Unable to create effect for shader  %@", shaderName);
        }
    }
    return self;
}


- (void)prepareToDraw {
    if (_programId == 0) { return; }
    glUseProgram(_programId);
}
- (void)bindAttributes:(NSMutableArray *)data {
    NSAssert(false, @"Default attr binding hasn't been overriden by effect");
}
- (void)configureUniforms:(GLKMatrix4)meshTransform {
    NSAssert(false, @"Default uniform initialization hasn't been overriden by effect");    
}
- (void)extractUniformLocations:(NSArray *)uniformNames
                      toStorage:(GLint *)storage {
    [EffectUtils extractUniformLocations:uniformNames fromProgram:_programId toStorage:storage];
}

- (void)dealloc {
    if (_programId) {
        glDeleteProgram(_programId);
        _programId = 0;
    }
}

@end
