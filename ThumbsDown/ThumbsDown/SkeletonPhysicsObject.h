//
//  ComposicePhysicsObject.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "PhysicsObject.h"

@interface SkeletonPhysicsObject : PhysicsObject

@property (strong, nonatomic) PhysicsObject *controllerObject;

-(id)initWithInnerParts:(NSMutableArray *)innerParts
       controllerObject:(PhysicsObject *)ctrlr
                 joints:(NSArray *)joints;

-(PhysicsObject *)physicsPartByName:(NSString *)name;
-(Joint *)jointById:(NSString *)jointId;

-(void)addJoint:(Joint *)joint;
-(void)removeJoint:(NSString *)jointId;

-(void)addPart:(PhysicsObject *)po;
-(void)removePart:(NSString *)partId;


@end
