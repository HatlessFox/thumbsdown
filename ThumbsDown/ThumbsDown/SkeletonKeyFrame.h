//
//  Animation.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/23/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GLKit;
#import "SkeletonPhysicsObject.h"
#import "KeyFrame.h"

@interface SkeletonKeyFrame : NSObject<KeyFrame>

@property (strong, nonatomic) NSString *objnName;

-(id)initWithDuration:(float)duration
        jointRotation:(NSDictionary *)jointRotations;

-(id)initWithDuration:(float)duration
        jointRotation:(NSDictionary *)jointRotations
        transposition:(GLKVector3)transposition
        ignoreChecks:(BOOL)ignoreChecks;

-(id)initWithDuration:(float)duration
        jointRotation:(NSDictionary *)jointRotations
        transposition:(GLKVector3)transposition;


-(BOOL)step:(float)elapsedTime;

@end
