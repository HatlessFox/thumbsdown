//
//  ViewController.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/16/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "ViewController.h"
#import "SimObjFactory.h"
#import "InputManager.h"
#import "PhysicsManager.h"
#import "GameManager.h"
#import "SimulationInfo.h"
#import "AnimationManager.h"

#import "DbgUtils.h"

@interface ViewController ()

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

@end


@implementation ViewController {
    BOOL _bulletRain;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    [InputManager setupGestureInputs:view];
    self.view.multipleTouchEnabled = TRUE;
    [self setupGL];
}

- (void)dealloc
{    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }

    // Dispose of any resources that can be recreated.
}


- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.65f, 0.65f, 0.65f, 1.0f);
    
    [GameManager setupWorld];
    [GameManager prepareForStart];
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update {
//DEBUG stuff
    [self handleBulletRain];
    //update obj cnt
    self.objCnt.text = [NSString stringWithFormat:@"%d", [SimObjFactory storedObjects].count];
//DEBUG stuff
/////
    
    [PhysicsManager updatePhysics];
    [GameManager update];
    [InputManager performCommands];
    [AnimationManager perfromAnimations:[PhysicsManager timeDelta]];
}


- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (SimulationObject *simObj in [SimObjFactory storedObjects]) {
        [simObj draw];
    }
   
  // [DbgUtils drawRoughtBoxes];
}

- (IBAction)fireBtnClick:(id)sender {
    [GameManager fire:[SimulationInfo controlledPlayerName]];
}

- (IBAction)jumpBtnClick:(id)sender {
    [GameManager jump:[SimulationInfo controlledPlayerName]];
}


- (IBAction)turnAroundClick:(id)sender {
    [GameManager turnAround:[SimulationInfo controlledPlayerName]];
}

- (IBAction)slowdownFPS:(id)sender {
    self.preferredFramesPerSecond = ((UISwitch *)sender).on ? 10 : 30;
}

- (IBAction)weaponSwitch:(id)sender {
    [InputManager changeWeapon];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [InputManager touchesMoved:touches withEvent:event];
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [InputManager touchesEnded:touches withEvent:event];
}


- (IBAction)onAimSwitched:(UISwitch *)sender {
    [InputManager reverseAimMode];
}

#pragma mark - Debug Methods
- (IBAction)addWBtnClick:(id)sender {
    static int enemyId = 2;
    SimulationObject *enemy = [SimObjFactory createObjectOfType:@"warrior"
        withId:[NSString stringWithFormat:@"enemy%d",++enemyId]];
    enemy.desiredWorldTransformMtx = GLKMatrix4Translate(
        enemy.worldTransformMtx, (random() % 20) - 10, 5, 0);
//    [enemy commitWorldTransform]; //don't care about collisions
}
- (void)handleBulletRain {
    static int bulletId = 0;
    if (!_bulletRain) { return; }
    
    SimulationObject *bullet = [SimObjFactory createObjectOfType:@"bullet"
        withId:[NSString stringWithFormat:@"bulletRain%d",++bulletId]];
        
    bullet.physicsSt.currVelocity = GLKVector3Make(0.0, -0.1 * (random() % 3 + 1), 0);
        
    bullet.desiredWorldTransformMtx = GLKMatrix4RotateZ(bullet.worldTransformMtx, GLKMathDegreesToRadians(90));
    bullet.desiredWorldTransformMtx = GLKMatrix4Translate(bullet.desiredWorldTransformMtx, 7, (random() % 200)/10 - 10, 0);
//    [bullet commitWorldTransform]; //don't care about collision
    
    bullet.physicsSt.ttl = 1000;
    [PhysicsManager addAlwaysMovingObject:bullet.name];
}
- (IBAction)bulletRainOn:(id)sender {
    _bulletRain = !_bulletRain;
}

@end
