//
//  GameStrategy.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "GameStrategy.h"
#import "SimObjFactory.h"

@implementation GameStrategy

-(id)initWithObjId:(NSString *)objId
           andType:(NSString *)type {
    self = [super init];
    if (self) {
        _name = objId;
        _type = type;
    }
    return self;
}

@end
