//
//  PhysicsObject.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/4/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "PhysicsObject.h"
#import "AxisAlignedBoundingBox.h"
#import "ObjectOrientedBoundingBox.h"
#import "SimObjFactory.h"

@implementation PhysicsObject {
    GLKMatrix4 _transposition;
}

//designated
-(id)init {
    self = [super init];
    if (self) {
        _particleEmitPoint = GLKVector3Make(0, 0, 0);
        _transposition = GLKMatrix4Identity;
    }
    return self;
}

-(id)initWithName:(NSString *)name {
    self = [self init];
    if (self) {
        _name = name;
    }
    return self;
}


-(void)setBoundingBox:(CollisionBox *)boundingBox {
    if (_boundingBox) { _boundingBox.transpositionProvider = nil; }
    _boundingBox = boundingBox;
    _boundingBox.transpositionProvider = self;
    
    [_boundingBox updateBoxTransposition];
}

-(void)setTransposition:(GLKMatrix4)transposition {
    _transposition = transposition;
    [self.boundingBox updateBoxTransposition];
}

-(GLKMatrix4)currentTransposition {
    return _transposition;
}

-(BOOL)checkIfPosInside:(GLKVector4)pos {
    return [_boundingBox checkIfPosInside:pos];
}

-(NSArray *)relatedParts {
    return @[self];
}

-(PhysicsObject *)controllerPart { return self; }


//FIXME: intrpduce mor erobust check/model
-(BOOL) hasRelatedParts {
    return self.relatedParts[0] != self;
}

-(BOOL)doesCollideWith:(PhysicsObject *)otherPhysObject {
    
    if ([self.ignoredCollisionsWithObjs containsObject:otherPhysObject.name] ||
        [otherPhysObject.ignoredCollisionsWithObjs containsObject:self.name]) {
        return NO;;
    }

    BOOL flag = NO;
    if (otherPhysObject.name && self.name) {
        flag = YES;
    }
    
    BOOL collisionDetectedRough = [self.boundingBox checkCollision:otherPhysObject.boundingBox];
    if (!collisionDetectedRough) { return NO; }
    if (![self hasRelatedParts] && ![otherPhysObject hasRelatedParts]) {
        return YES;
    }
    
    for (PhysicsObject *selfPart in [self relatedParts]) {
        for (PhysicsObject *otherPart in [otherPhysObject relatedParts]) {
            if (!selfPart.boundingBox || !otherPart.boundingBox) {
                continue;
            }
            
            if([selfPart.boundingBox checkCollision:otherPart.boundingBox]) {
                return YES;
            }
        }
    }
    
    return NO;
}

-(void)prepareForCollisionDetection {
    [self.boundingBox updateBoxTransposition];
}


@end
