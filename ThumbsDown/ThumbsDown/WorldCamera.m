//
//  WorldCamera.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "WorldCamera.h"
#import "SimObjFactory.h"
#import "PDController.h"

@interface WorldCamera ()

@property (strong, nonatomic) PDController *pdc;
@property (strong, nonatomic) PDController *zoomPdc;

@end


@implementation WorldCamera {
    float _zoomFactor;
}

-(GLKMatrix4)viewMatrix {
    GLKMatrix4 boundObjectTransform = GLKMatrix4Identity;
    GLKVector3 value = _pdc.currentValue;
    boundObjectTransform.m30 = value.x;
    boundObjectTransform.m31 = value.y;
    boundObjectTransform.m32 = value.z;
    
    return GLKMatrix4Multiply(boundObjectTransform, _viewMatrix);
}

-(id)init {
    self = [super init];
    if (self) {
        _viewMatrix = GLKMatrix4Identity;
        _projectionMatrix = GLKMatrix4Identity;
        _pdc = [[PDController alloc] initWithCurrentPosition:GLKVector3Make(0, 0, 0) daming:1 springConst:2];
        _zoomPdc = [[PDController alloc] initWithCurrentPosition:GLKVector3Make(0, 0, 1) daming:1 springConst:2];
        _zoomFactor = 10;
    }
    return self;
}

-(void)setZoomFactor:(CGFloat)zoomFactor {
    _zoomPdc.desiredValue = GLKVector3Make(0, 0, zoomFactor);
    //_zoomFactor = zoomFactor;
}


-(CGFloat)zoomFactor {
    return _zoomPdc.currentValue.z;
    //return _zoomFactor;
}

-(void)handleBoundObjTransform:(NSNotification *)notification {
    GLKMatrix4 transform =
        ((SimulationObject *)notification.object).worldTransformMtx;
    
    //bool isInv = true;
    //GLKMatrix4Invert(transform, &isInv);
    
    //use ad-hoc invert for performance
    //rotations of bound object are not expected
    transform.m30 *= -1.0f;
    transform.m31 *= -1.0f;
    transform.m32 *= -1.0f;
    
    
    //if (fabs(transform.m30 - _boundObjectTransform.m30) > 0.01 ||
     //   fabs(transform.m31 - _boundObjectTransform.m31) > 0.01) {
   // }
    
    [_pdc setDesiredValue:GLKVector3Make(transform.m30, transform.m31, transform.m32)];
}

+(void)bindToObject:(NSString *)objName {
    SimulationObject *obj = [SimObjFactory createdObjectById:objName];
    if (!obj) { return; }

    [[NSNotificationCenter defaultCenter] removeObserver:[WorldCamera instance]];
    [[NSNotificationCenter defaultCenter] addObserver:[WorldCamera instance]
        selector:@selector(handleBoundObjTransform:)
        name:[NSString stringWithFormat:@"%@Transform", objName]
        object:nil];
    
    [WorldCamera instance].pdc = [[PDController alloc] initWithCurrentPosition:
        GLKVector3Make(-obj.worldTransformMtx.m30,
                       -obj.worldTransformMtx.m31,
                       -obj.worldTransformMtx.m32) daming:1.5 springConst:2];
}

+(void)update:(float)timeDelta {
    WorldCamera *wc = [WorldCamera instance];
    [wc.pdc update:timeDelta];
    [wc.zoomPdc update:timeDelta];
    [WorldCamera updateCamera];
}


+(WorldCamera *)instance {
    static WorldCamera *instance;
    if (!instance) {
        instance = [WorldCamera new];
    }
    return instance;
}

+(void)applyTransform:(GLKMatrix4)transform {
    WorldCamera *cam = [WorldCamera instance];
    cam.viewMatrix = GLKMatrix4Multiply(cam.viewMatrix, transform);
}

+(void)moveHoriz:(float)delta {
    WorldCamera *cam = [WorldCamera instance];
    cam.viewMatrix = GLKMatrix4Translate(cam.viewMatrix, delta, 0.0f, 0.0f);
}

+(void)moveDepth:(float)depth {
    WorldCamera *cam = [WorldCamera instance];
    cam.viewMatrix = GLKMatrix4Translate(cam.viewMatrix, 0.0f, 0.0f, depth);
}

+(void)updateCamera {
    WorldCamera *cam = [WorldCamera instance];
    CGRect frame = [[UIScreen mainScreen] bounds];
    float aspect = fabsf((float)frame.size.height / (float)frame.size.width);
    cam.projectionMatrix =
    GLKMatrix4MakeOrtho(-cam.zoomFactor * aspect, cam.zoomFactor * aspect, -cam.zoomFactor, cam.zoomFactor, 0.01, 20);
}

+(void)rotateY:(float)degrees {
    WorldCamera *cam = [WorldCamera instance];
    cam.viewMatrix = GLKMatrix4RotateY(
        cam.viewMatrix, GLKMathDegreesToRadians(degrees));
}


+(GLKVector4)toProjView:(GLKVector4)vector {
    WorldCamera *cam = [WorldCamera instance];
    GLKVector4 res = GLKMatrix4MultiplyVector4(cam.viewMatrix, vector);
    return GLKMatrix4MultiplyVector4(cam.projectionMatrix, res);

}

+(void)pullback:(GLKVector3)delta withZoom:(CGFloat)zoom {
    [self pullback:delta];
    [WorldCamera instance].zoomPdc.currentValue = GLKVector3Make(0, 0, zoom);
}

+(void)pullback:(GLKVector3)delta {
    PDController *pdc = [WorldCamera instance].pdc;
    pdc.currentValue = GLKVector3Add(pdc.currentValue, delta);
    
}

@end
