//
//  ObjectOrientedBoundingBox.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/31/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "ObjectOrientedBoundingBox.h"
#import "CollisionBox.h"
#import "CompositeCollisionBox.h"
#import "AxisAlignedBoundingBox.h"

@implementation ObjectOrientedBoundingBox {
    float _minX, _maxX, _minY, _maxY;
}

#pragma mark - Collisions

+(BOOL)checkCollisionWithBase:(RectangleCollisionBox *)base
                       testee:(RectangleCollisionBox *)testee
                    transform:(GLKMatrix4)testeeToBaseMtx {
    
    //convert testee's edge point coordinates to base coord system
    
    CGSize testeeSize = testee.bounds.size;
    GLKVector4 testeeLB = GLKMatrix4MultiplyVector4(testeeToBaseMtx,
        GLKVector4Make(0, 0, 0.0f, 1.0f));
    GLKVector4 testeeLT = GLKMatrix4MultiplyVector4(testeeToBaseMtx,
        GLKVector4Make(0, testeeSize.height, 0.0f, 1.0f));
    GLKVector4 testeeRB = GLKMatrix4MultiplyVector4(testeeToBaseMtx,
        GLKVector4Make(testeeSize.width, 0, 0.0f, 1.0f));
    GLKVector4 testeeRT = GLKMatrix4MultiplyVector4(testeeToBaseMtx,
        GLKVector4Make(testeeSize.width, testeeSize.height, 0.0f, 1.0f));
    
    //determine bounds
    float minX = MIN(testeeLB.x, MIN(testeeLT.x, MIN(testeeRB.x, testeeRT.x)));
    float maxX = MAX(testeeLB.x, MAX(testeeLT.x, MAX(testeeRB.x, testeeRT.x)));
    float minY = MIN(testeeLB.y, MIN(testeeLT.y, MIN(testeeRB.y, testeeRT.y)));
    float maxY = MAX(testeeLB.y, MAX(testeeLT.y, MAX(testeeRB.y, testeeRT.y)));
	
	float minXBase = MIN(0, base.bounds.size.width);
	float maxXBase = MAX(0, base.bounds.size.width);
	float minYBase = MIN(0, base.bounds.size.height);
	float maxYBase = MAX(0, base.bounds.size.height);
	
    return
        [self isOverlapsP1S:minXBase andP1E:maxXBase withP2S:minX andP2E:maxX] &&
        [self isOverlapsP1S:minYBase andP1E:maxYBase withP2S:minY andP2E:maxY];
}

-(BOOL)checkBoxIntersection:(RectangleCollisionBox *)actor {
    if (!CGRectIntersectsRect(self.roughBoundingRect, actor.roughBoundingRect)){
        return NO;
    }
    
    GLKMatrix4 selfTransition = self.boxTransposition;
    GLKMatrix4 otherTransition = actor.boxTransposition;
    
    //FIXME: use projections
    bool isInv;
    GLKMatrix4 selfTranslInv = GLKMatrix4Invert(selfTransition, &isInv);
    if ([ObjectOrientedBoundingBox checkCollisionWithBase:self testee:actor
                                                transform:GLKMatrix4Multiply(selfTranslInv, otherTransition)] == NO) {
        return NO;
    }
    
    GLKMatrix4 otherTranslInv = GLKMatrix4Invert(otherTransition, &isInv);
    return [ObjectOrientedBoundingBox checkCollisionWithBase:actor testee:self
                transform:GLKMatrix4Multiply(otherTranslInv, selfTransition)];
}

-(BOOL)checkCollision:(CollisionBox *)actor {
    //TODO refactor
    if ([actor isMemberOfClass:[CompositeCollisionBox class]]) {
        return [actor checkCollision:self];
    }
    
    if (![actor isKindOfClass:[RectangleCollisionBox class]]) {
        NSLog(@"Can't check collision");
        return NO;
    }
    
    return [self checkBoxIntersection:(RectangleCollisionBox *)actor];
}



-(float)maxX { return _maxX; }
-(float)minX { return _minX; }
-(float)maxY { return _maxY; }
-(float)minY { return _minY; }


-(void)updateBoxTransposition {
    [super updateBoxTransposition];
    
    GLKMatrix4 boxTransposition = self.boxTransposition;
    CGSize boxSize = self.bounds.size;
    
    GLKVector4 lb = GLKMatrix4MultiplyVector4(boxTransposition, RCB_zeroVector4);
    GLKVector4 lt = GLKMatrix4MultiplyVector4(boxTransposition,
        GLKVector4Make(0, boxSize.height, 0.0f, 1.0f));
    GLKVector4 rb = GLKMatrix4MultiplyVector4(boxTransposition,
        GLKVector4Make(boxSize.width, 0, 0.0f, 1.0f));
    GLKVector4 rt = GLKMatrix4MultiplyVector4(boxTransposition,
        GLKVector4Make(boxSize.width, boxSize.height, 0.0f, 1.0f));
    
    _minX = MIN(lb.x, MIN(lt.x, MIN(rb.x, rt.x)));
    _maxX = MAX(lb.x, MAX(lt.x, MAX(rb.x, rt.x)));
    _minY = MIN(lb.y, MIN(lt.y, MIN(rb.y, rt.y)));
    _maxY = MAX(lb.y, MAX(lt.y, MAX(rb.y, rt.y)));
}

-(CGRect)roughBoundingRect { return CGRectMake(_minX, _minY, _maxX - _minX, _maxY - _minY); }

@end
