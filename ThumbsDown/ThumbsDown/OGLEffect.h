//
//  OGLEffect.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OGLServerBuffer.h"
#import "GLKit/GLKit.h"
#import "OGLBuffer.h"

@interface OGLEffect : NSObject
- (id)initWithShader:(NSString *)shaderName;
- (id)initWithShader:(NSString *)shaderName
       andAttributes:(NSDictionary *)attrs;

- (void)prepareToDraw;
- (void)bindAttributes:(id<OGLBuffer>)buffer;
- (void)configureUniforms:(GLKMatrix4)meshTransform;
- (void)extractUniformLocations:(NSArray *)uniformNames
                      toStorage:(GLint *)storage;

- (void)dealloc;
@end
