//
//  HumanSkeletonConstants.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/2/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "HumanSkeletonConstants.h"

//body
NSString * const hsNeckId = @"Neck";
NSString * const hsLoinId = @"Loin";

//arms
NSString * const hsRightShoulderId = @"RightShoulder";
NSString * const hsRightElbowId = @"RightElbow";
NSString * const hsRightWristId = @"RightWrist";

NSString * const hsLeftShoulderId = @"LeftShoulder";
NSString * const hsLeftElbowId = @"LeftElbow";

//legs
NSString * const hsRightHipId = @"RightHip";
NSString * const hsRightKneeId = @"RightKnee";
NSString * const hsRightAnkleId = @"RightAnkle";

NSString * const hsLeftHipId = @"LeftHip";
NSString * const hsLeftKneeId = @"LeftKnee";
NSString * const hsLeftAnkleId = @"LeftAnkle";


@implementation HumanSkeletonConstants

@end
