//
//  WarriorMoveCommand.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Command.h"
@import GLKit;

@interface MoveCommand : NSObject<Command>

-(id)initWithActorName:(NSString *)actorName
           translation:(GLKVector3)translation;

@end
