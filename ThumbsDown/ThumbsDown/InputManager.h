//
//  InputManager.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Command.h"

@interface InputManager : NSObject

@property (assign, nonatomic) BOOL aimOn;
+(InputManager*)instance;


+(void)performCommands;
+(void)setupGestureInputs:(UIView *)mainView;
+(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
+(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
+(void)disableMovement;

+(void)reverseAimMode;
+(void)submitCommand:(id<Command>)cmd;
+(void)recoil:(NSString *)playerId;
+(void)changeWeapon;


@end
