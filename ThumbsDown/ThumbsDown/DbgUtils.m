//
//  OGLUtils.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "DbgUtils.h"
#import "ColoredDrawEffect.h"
#import "OGLServerBuffer.h"
#import "SimObjFactory.h"
#import "SimulationInfo.h"
#import "CompositeCollisionBox.h"
#import "SkeletonPhysicsObject.h"
#import "TextureDrawEffect.h"

@implementation DbgUtils

+(void)logCurrentOGLError {
    GLenum errCode = glGetError();
    switch (errCode) {
        case GL_INVALID_ENUM: NSLog(@"Invalid enum"); break;
        case GL_INVALID_VALUE: NSLog(@"Invalid value"); break;
        case GL_INVALID_OPERATION: NSLog(@"Invalid operation"); break;
        case GL_NO_ERROR: NSLog(@"No error"); break;
        default: NSLog(@"Unknown error"); break;
    }
}

+(void)updateData:(NSMutableData *)data
       andElemCnt:(GLint *)elems
  withBoundingBox:(CollisionBox *)box
            level:(int)level
         andColor:(GLKVector3)color {
    
    if ([box isMemberOfClass:[CompositeCollisionBox class]]) {
        CompositeCollisionBox *compBox = (CompositeCollisionBox *)box;
        for (CollisionBox *part in compBox.parts) {
            [self updateData:data andElemCnt:elems withBoundingBox:part level:level andColor:color];
        }
        return;
    }
    
    GLfloat boxCoords[6*6] = {
        box.minX, box.minY, level, color.r, color.b, color.g,
        box.maxX, box.maxY, level, color.r, color.b, color.g,
        box.maxX, box.minY, level, color.r, color.b, color.g,
        
        box.minX, box.minY, level, color.r, color.b, color.g,
        box.maxX, box.maxY, level, color.r, color.b, color.g,
        box.minX, box.maxY, level, color.r, color.b, color.g,
    };
    
    *elems += 6;
    [data appendBytes:boxCoords length:sizeof(boxCoords)];
}

+(void)drawRoughtBoxes {
    ColoredDrawEffect *_eff = [[ColoredDrawEffect alloc] initWithOpacity:0.5];
    OGLServerBuffer *_buffer = [[OGLServerBuffer alloc] initWithBufferType:GL_ARRAY_BUFFER
                                                                 andStride:24];
    
    GLKVector3 blackColor = GLKVector3Make(0, 0, 0);
    GLKVector3 redColor = GLKVector3Make(1, 0, 0);
    GLKVector3 greenColor = GLKVector3Make(0, 0, 1);
    GLint elemntsToDraw = 0;
    NSMutableData *dataToDraw = [NSMutableData new];
    
    for (SimulationObject *simObj in [SimObjFactory storedObjects]) {
        CollisionBox *boundingBox = simObj.physicsSt.physObject.boundingBox;
        [self updateData:dataToDraw andElemCnt:&elemntsToDraw withBoundingBox:boundingBox level:2 andColor:blackColor];
        if ([simObj.physicsSt.physObject isMemberOfClass:[SkeletonPhysicsObject class]]) {
            SkeletonPhysicsObject *skelPO = (SkeletonPhysicsObject *)simObj.physicsSt.physObject;
            for (PhysicsObject *po in [skelPO relatedParts]) {
                if ([po.name isEqualToString:@"Weapon"]) {
                    [self updateData:dataToDraw andElemCnt:&elemntsToDraw withBoundingBox:po.boundingBox level:5 andColor:greenColor];
                } else {
                    [self updateData:dataToDraw andElemCnt:&elemntsToDraw withBoundingBox:po.boundingBox level:3 andColor:redColor];
                }
            }
        }
        if ([simObj.type isEqualToString:@"bullet"] || [simObj.type isEqualToString:@"bulletFraction"]) {
            CollisionBox *bb = simObj.physicsSt.physObject.boundingBox;
            [self updateData:dataToDraw andElemCnt:&elemntsToDraw withBoundingBox:bb level:6 andColor:greenColor];
        }
    }
    
    [_buffer updateDataFrom:dataToDraw.bytes withElemsCount:elemntsToDraw andDrawMode:GL_STATIC_DRAW];
    
    [_eff prepareToDraw];
    [_eff configureUniforms:GLKMatrix4Identity];
    [_eff bindAttributes:_buffer];
    [_buffer drawElemnts:elemntsToDraw withMode:GL_TRIANGLES
            startingFrom:0];
}

+(void)drawTextureWithName:(NSString *)name bounds:(CGRect)bounds {
    OGLEffect *eff = [[TextureDrawEffect alloc] initWithImageName:@"EW.jpg"];
    OGLServerBuffer *buffer = [[OGLServerBuffer alloc] initWithBufferType:GL_ARRAY_BUFFER
                                                                andStride:5 * sizeof(float)];
    
    //init buffer data
    GLint elementsToDraw = 6;
    
    CGPoint bo = bounds.origin;
    CGSize bs = bounds.size;
    
    GLfloat boxCoords[6*5] = {
        bo.x, bo.y, 5,                       0,0,
        bo.x, bo.y + bs.height, 5,           0,5,
        bo.x + bs.width, bo.y + bs.height, 5,5,5,

        bo.x, bo.y, 5,                       0,0,
        bo.x + bs.width, bo.y, 5,            5,0,
        bo.x + bs.width, bo.y + bs.height, 5,5,5
    };
    
    [buffer updateDataFrom:boxCoords withElemsCount:elementsToDraw andDrawMode:GL_STATIC_DRAW];
    [eff prepareToDraw];
    [eff configureUniforms:GLKMatrix4Identity];
    [eff bindAttributes:buffer];
    [buffer drawElemnts:elementsToDraw withMode:GL_TRIANGLES startingFrom:0];
}


@end
