//
//  Mesh.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Mesh.h"

@implementation Mesh {
    GLenum _drawMode;
}


-(id)initWithBuffer:(OGLServerBuffer *)buffer
   numberOfElements:(GLuint)elemsCnt
         StartingFrom:(GLuint)offset
             WithEffect:(OGLEffect *)effect
           drawMode:(GLenum)mode{
    self = [super init];
    if (self) {
        _oglBuffer = buffer;
        _elementsCount = elemsCnt;
        _offset = offset;
        _effect = effect;
        _drawMode = mode;
    }
    return self;
}


//TODO bind uniforms
-(void)draw:(GLKMatrix4)meshTransform {
    [self.effect prepareToDraw];
    
    GLKMatrix4 combinedMeshTransform = GLKMatrix4Multiply(
        meshTransform, [self currentTranslation]
    );
    [self.effect configureUniforms:combinedMeshTransform];
    [self.effect bindAttributes:self.oglBuffer];
    [self.oglBuffer drawElemnts:self.elementsCount withMode:_drawMode
                   startingFrom:self.offset];
}

-(GLKMatrix4)currentTranslation {
    return _transpositionProvider ? [_transpositionProvider currentTransposition] :
                                  GLKMatrix4Identity;
}

@end
