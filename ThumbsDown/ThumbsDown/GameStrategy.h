//
//  GameStrategy.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameStrategy : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *type;

-(id)initWithObjId:(NSString *)objId
           andType:(NSString *)type;

@end
