//
//  Loader.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "SimObjFactory.h"
#import "ColoredDrawEffect.h"
#import "MeshComposite.h"
#import "OGLServerBuffer.h"
#import "AxisAlignedBoundingBox.h"
#import "ObjectOrientedBoundingBox.h"
#import "CompositeCollisionBox.h"
#import "PhysicsManager.h"
#import "BonePhysicsObject.h"
#import "Joint.h"
#import "SkeletonPhysicsObject.h"
#import "TextureDrawEffect.h"
#import "HumanSkeletonConstants.h"
#import "Pistol.h"
#import "WarriorGameStrategy.h"
#import "AK.h"
#import "Knife.h"
#import "Shotgun.h"

//--------------------------------------
//MODELS

const int MESH_RECT_ELEMS = 6;
#define MESH_RECT(x,y,w, h,z,r,g,b) \
    x,   y,   z, r, g, b, \
    x,   y+h, z, r, g, b, \
    x+w, y+h, z, r, g, b, \
    x+w, y+h, z, r, g, b, \
    x+w, y,   z, r, g, b, \
    x,   y,   z, r, g, b

const int MESH_RECT_TEX_ELEMS = 6;
#define MESH_RECT_TEX(x,y,w,h,z,hcnt, wcnt) \
    x,   y,   z, 0, 0, 0,       \
    x,   y+h, z, 0, wcnt, 0,    \
    x+w, y+h, z, hcnt, wcnt, 0, \
    x+w, y+h, z, hcnt, wcnt, 0, \
    x+w, y,   z, hcnt, 0, 0,   \
    x,   y,   z, 0, 0, 0


const int MESH_TRAPEZ_ELEMS = 6;
#define MESH_TRAPEZ_HORIZ_TOPLESS(x,y,wBot, wTop, h, z,r,g,b) \
    x,   y,   z, r, g, b, \
    x + (wBot - wTop) / 2,   y + h,   z, r, g, b, \
    x + (wBot + wTop) / 2,   y + h,   z, r, g, b, \
    x,   y,   z, r, g, b, \
    x + (wBot + wTop) / 2,   y + h,   z, r, g, b, \
    x + wBot,   y,   z, r, g, b

#define MESH_TRAPEZ_HORIZ_BOTLESS(x,y,wBot, wTop, h, z,r,g,b) \
    x,   y + h,   z, r, g, b, \
    x + (wTop - wBot) / 2,   y,   z, r, g, b, \
    x + (wBot + wTop) / 2,   y,   z, r, g, b, \
    x,   y + h,   z, r, g, b, \
    x + (wBot + wTop) / 2,   y,   z, r, g, b, \
    x + wTop,   y + h,   z, r, g, b

#define MESH_TRAPEZ_VERT_RIGHTLESS(x,y,wBot, wTop, h, z,r,g,b) \
    x,   y,   z, r, g, b, \
    x + h, y + (wBot - wTop) / 2,  z, r, g, b, \
    x + h, y + (wBot + wTop) / 2,  z, r, g, b , \
    x,   y,   z, r, g, b, \
    x + h, y + (wBot + wTop) / 2,  z, r, g, b, \
    x, y + wBot,   z, r, g, b

#define MESH_TRAPEZ_VERT_LEFTLESS(x,y,wBot, wTop, h, z,r,g,b) \
    x,   y,   z, r, g, b, \
    x - h, y + (wBot - wTop) / 2,  z, r, g, b, \
    x - h, y + (wBot + wTop) / 2,  z, r, g, b , \
    x,   y,   z, r, g, b, \
    x - h, y + (wBot + wTop) / 2,  z, r, g, b, \
    x, y + wBot,   z, r, g, b



//Warrior
const NSString * WARRIOR_ID = @"warrior";
const GLuint WARRIOR_ELEMCNT = 15*MESH_RECT_ELEMS + 1*3 + 9*MESH_TRAPEZ_ELEMS;
const GLuint WARRIOR_STRIDE = sizeof(GLfloat) * 6;
GLfloat warrVertexData[WARRIOR_ELEMCNT*6] =
{
    // Data layout for each line below is:
    // positionX, positionY, positionZ, r, g, b,
    
    //// Core
    //Torso
    MESH_TRAPEZ_HORIZ_TOPLESS(0.0,0.55, 0.3, 0.2, 0.05, 0.5, 0.0,0.2,0),
    MESH_RECT(0.0f, 0.2f, 0.3f, 0.35f, 0.5f, 0.0f, 0.2f, 0.0f),
    MESH_TRAPEZ_HORIZ_BOTLESS(0.0,0.0, 0.2, 0.3, 0.2, 0.5, 0.0,0.2,0),
    //// Head
    // Head
    MESH_RECT(0.05f, 0.05f, 0.15f, 0.20f, 0.5f, 0.7f, 0.7f, 0.0f),
    MESH_TRAPEZ_VERT_RIGHTLESS(0.2,0.1, 0.2, 0.1, 0.05, 0.5,0.7,0.7,0),
    MESH_TRAPEZ_HORIZ_TOPLESS(0.05,0.25, 0.2, 0.1, 0.05, 0.5,0.7,0.7,0),
    
    // Neck
    MESH_RECT(0.10f, 0.0f, 0.10f, 0.05f, 0.5f, 0.7f, 0.7f, 0.0f),
    // Chin
    MESH_TRAPEZ_VERT_LEFTLESS(0.05,0.05, 0.05, 0.02, 0.03, 0.5f, 0.7f, 0.7f, 0.0f),

    // Nouse
    0.0,   .15,   .5, .7, .7, 0,
    0.05,  .15, .5, .7, .7, 0,
    .05, .23, .5, .7, .7, 0,
    // Eye
    MESH_TRAPEZ_VERT_RIGHTLESS(0.1,0.2,0.03, 0.01, 0.02, 0.51f, 0.8f, 0.8f, 0.8f),
    MESH_RECT(0.07f, 0.20f, 0.03f, 0.03f, 0.51f, 0.8f, 0.0f, 0.0f),
    // Mouse
    MESH_RECT(0.05f, 0.11f, 0.04f, 0.02f, 0.51f, 0.3f, 0.0f, 0.0f),
    
    //// Hips
    MESH_TRAPEZ_HORIZ_TOPLESS(0.0, 0.0, 0.4, 0.2, 0.1, 0.5,0.0,0.2,0),

    //// Left Arm
    //Left Arm High
    MESH_RECT(0.0, 0.0, 0.2, 0.45, 0.3, 0, 0.5f, 0),
    //Left Arm Low
    MESH_RECT(0.0, 0.0, 0.12, 0.35, 0.2, 0.7, 0.7, 0.0),

    
    //// Left Leg
    //Left Leg High
    MESH_RECT(0.0f, 0.0f, 0.2f, 0.3f, 0.3f, 0.0f, 0.2f, 0.0f),
    //Left Leg Low
    MESH_RECT(0.0f, 0.0f, 0.15f, 0.25f, 0.3f, 0.7f, 0.7f, 0.0f),
    //Left Shoe
    MESH_RECT(0.0f, 0.0f, 0.30f, 0.2f, 0.3f, 0.0f, 0.0f, 0.0f),
    
    //// Right Leg
    //Right Leg High
    MESH_RECT(0.0f, 0.0f, 0.2f, 0.3f, 1.0f, 0.0f, 0.2f, 0.0f),
    //Right Leg Low
    MESH_RECT(0.0f, 0.0f, 0.15f, 0.25f, 1.0f, 0.7f, 0.7f, 0.0f),
    //Right Shoe
    MESH_RECT(0.0f, 0.0f, 0.30f, 0.2f, 1.0f, 0.0f, 0.0f, 0.0f),
    
    
    //// Right Arm
    //Right Arm High
    MESH_TRAPEZ_HORIZ_TOPLESS(0.03, 0.42, 0.15, 0.07, 0.03, 1.0, 0, 0.5f, 0),
    MESH_RECT(0.03, 0.03, 0.15, 0.39, 1.0, 0, 0.5f, 0),
    MESH_TRAPEZ_HORIZ_BOTLESS(0.03, 0.0, 0.07, 0.15, 0.03, 1.0, 0, 0.5, 0),
    
    //Right Arm Low
    MESH_RECT(0.0, 0.0, 0.12, 0.45, 1.1, 0.7, 0.7, 0.0),
};

//Pistol
const NSString * PISTOL_ID = @"pistol";
const GLuint PISTOL_ELEMCNT = MESH_RECT_ELEMS;
const GLuint PISTOL_STRIDE = 24;
GLfloat pistolVertexData[PISTOL_ELEMCNT*6] =
{
    MESH_RECT(0.0, 0.0, 0.35, 0.1, 1.2, 0, 0, 0)
};

//AK
const NSString * AK_ID = @"ak";
const GLuint AK_ELEMCNT = 3 * MESH_RECT_ELEMS;
const GLuint AK_STRIDE = 24;
GLfloat akVertexData[AK_ELEMCNT*6] =
{
    MESH_RECT(0.0, 0.0, 0.5, 0.15, 0.6, 0.3, 0.3, 0),
    MESH_RECT(-0.2, 0.05, 0.2, 0.1, 0.6, 0.4, 0.4, 0.4),
    MESH_RECT(0.07, -0.2, 0.1, 0.2, 0.6, 0.2, 0.2, 0.2)
};

//Shotgun
const NSString * SHOTGUN_ID = @"shotgun";
const GLuint SHOTGUT_ELEMCNT = 2 * MESH_RECT_ELEMS;
const GLuint SHOTGUN_STRIDE = 24;
GLfloat shotgunVertexData[SHOTGUT_ELEMCNT*6] =
{
    MESH_RECT(0.0, 0.0, 0.5, 0.15, 0.6, 0.3, 0.3, .1),
    MESH_RECT(-0.2, 0.05, 0.2, 0.1, 0.6, 0.4, 0.4, 0.4)
};

//AK
const NSString * KNIFE_ID = @"knife";
const GLuint KNIFE_ELEMCNT = 3 * MESH_RECT_ELEMS;
const GLuint KNIFE_STRIDE = 24;
GLfloat knifeVertexData[KNIFE_ELEMCNT*6] =
{
    MESH_RECT(0.0, 0.0, 0.15, 0.3, 1.2, 0.6, 0.5, 0.5),
    MESH_RECT(0.07, 0.3, 0.08, 0.05, 1.2, 0.6, 0.5, 0.5),
    MESH_RECT(0.07, -0.2, 0.1, 0.2, 1.2, 0.2, 0.2, 0.2)
};

//Map
const NSString * MAP_ID = @"static_map";
const GLuint MAP_ELEMCNT = 17 * 6;
const GLuint MAP_STRIDE = 24;
GLfloat mapVertexData[MAP_ELEMCNT*6] =
{
    // Data layout for each line below is:
    // positionX, positionY, positionZ, r, g, b,
    
    //// Gallery Hi
    MESH_RECT_TEX(-3.0f, -1.0f, 6.0f, 0.5f, 0.0f, 10, 2),
    MESH_RECT_TEX(-3.0f, -0.5f, 1.5f, 2.0f, 0.0f, 3, 6),
    
    //// Gallery Low
    MESH_RECT_TEX(-8.0f, -5.5f, 15.0f, 1.0f, 0.0f, 1, 1),
    MESH_RECT_TEX(-8.0f, -4.5f, 2.5f, 1.5f, 0.0f, 1,1),
    MESH_RECT_TEX(+7.0f, -5.5f, 2.0f, 2.0f, 0.0f, 1,1),
    MESH_RECT_TEX(+9.0f, -4.5f, 0.5f, 0.3f, 0.0f, 1,1),
    
    //// Ground right
    MESH_RECT_TEX(-30.0f, -14.0f,  17.0f,6.0f, 0.0f, 1,1),
    MESH_RECT_TEX(-30.0f, -8.0f, 20.0f, 2.0f, 0.0f, 1,1),
    MESH_RECT_TEX(-13.0f, -14.0f, 5.0f, 1.5f, 0.0f, 1,1),
    
    //Ground left
    MESH_RECT_TEX(-5.0f, -12.0f,  5.0f,3.0f, 0.0f, 1,1),
    MESH_RECT_TEX(3.0f, -14.0f, 17.0f, 2.3f, 0.0f, 1,1),
    
    MESH_RECT_TEX(11.0f, -12.0f, 2.0f, 7.0f, 0.0f, 1,1),
    MESH_RECT_TEX(10.5f, -8.0f,  0.5f,0.3f, 0.0f, 1,1),
    
    //////Border
    MESH_RECT_TEX(-30.0f, -15.0f, 60.0f, 1.0f, 0.0f, 1,1),
    MESH_RECT_TEX(-30.0f, 7.0f, 60.0f, 1.0f, 0.0f, 1,1),
    MESH_RECT_TEX(-30.0f, -15.0f, 1.0f,23.0f,  0.0f, 1,1),
    MESH_RECT_TEX(30.0f, -15.0f,  1.0f,23.0f, 0.0f, 1,1)
};

//bullet
const NSString * BULLET_ID = @"bullet";
const GLuint BULLET_ELEMCNT = 6;
const GLuint BULLET_STRIDE = 24;
GLfloat bulletVertexData[BULLET_ELEMCNT*6] =
{
    // Data layout for each line below is:
    // positionX, positionY, positionZ, r, g, b,
    ////Bullet itself
    -0.15f, 0.0f, 1.4f,         0.25f,0.25f,0.25f,
    0.0f, 0.05f, 1.4f,         0.25f,0.25f,0.25f,
    0.0f, -0.05f, 1.4f,        0.25f,0.25f,0.25f,
    
    ////trace
    0.0f, 0.03f, 0.0f,        0.75f,0.75f,0.75f,
    0.65f, 0.0f, 0.0f,        0.75f,0.75f,0.75f,
    0.0f, -0.03f, 0.0f,        0.75f,0.75f,0.75f
};

//
const NSString * BFRACTION_ID = @"bulletFraction";
const GLuint BFRACTION_ELEMCNT = MESH_RECT_ELEMS;
const GLuint BFRACTION_STRIDE = 24;
GLfloat bFractionVertexData[BFRACTION_ELEMCNT*6] =
{
    MESH_RECT(-0.05f,-0.05f, 0.05f, 0.05f, 1.4f, 0.25f,0.25f,0.25f)
};


@implementation SimObjFactory {
    NSSet *_supportedObjects;
    //NSDictionary *_meshData;       // type -> vtx data
    NSMutableDictionary *_simObjRegister; // id   -> sim object
    OGLServerBuffer *_commonBuffer;
}


-(void)setupSupportedObjects {
    _supportedObjects = [[NSSet alloc] initWithArray:@[WARRIOR_ID, MAP_ID, BULLET_ID, PISTOL_ID, AK_ID, KNIFE_ID, BFRACTION_ID, SHOTGUN_ID]];
}

-(id)init {
    self = [super init];
    if (self) {
        _simObjRegister = [NSMutableDictionary new];
        NSMutableData *data = [[NSMutableData alloc] initWithCapacity:(sizeof(warrVertexData)+sizeof(mapVertexData)+sizeof(bulletVertexData) + sizeof(pistolVertexData) + sizeof(akVertexData) +
            sizeof(knifeVertexData) + sizeof(bFractionVertexData) + sizeof(shotgunVertexData))];
        
        [data appendBytes:warrVertexData length:sizeof(warrVertexData)];
        [data appendBytes:mapVertexData length:sizeof(mapVertexData)];
        [data appendBytes:bulletVertexData length:sizeof(bulletVertexData)];
        [data appendBytes:pistolVertexData length:sizeof(pistolVertexData)];
        [data appendBytes:akVertexData length:sizeof(akVertexData)];
        [data appendBytes:knifeVertexData length:sizeof(knifeVertexData)];
        [data appendBytes:bFractionVertexData length:sizeof(bFractionVertexData)];
        [data appendBytes:shotgunVertexData length:sizeof(shotgunVertexData)];
        
        _commonBuffer = [[OGLServerBuffer alloc] initWithBufferType:GL_ARRAY_BUFFER
                                                    andStride:MAP_STRIDE];
        [_commonBuffer updateDataFrom:data.bytes withElemsCount:data.length andDrawMode:GL_STATIC_DRAW];
    }
    return self;
}

+(SimObjFactory *)instance {
    static SimObjFactory *instance;
    if (!instance) {
        instance = [SimObjFactory new];
        [instance setupSupportedObjects];
    }
    return instance;
}

-(NSMutableDictionary *)simObjRegister { return _simObjRegister; }

+(SimulationObject *)createdObjectById:(NSString *)objId {
    return [[SimObjFactory instance] simObjRegister][objId];
}

+(void)removeObject:(NSString *)objId {
    [PhysicsManager forgetObject:objId];
    [[[SimObjFactory instance] simObjRegister] removeObjectForKey:objId];
}

+(void)removeObjects:(id<NSFastEnumeration>)objIds {
    for (NSString *objId in objIds) {
        [self removeObject:objId];
    }
}

+(NSArray *)storedObjects {
    return [[[SimObjFactory instance] simObjRegister] allValues];
}

-(BOOL)checkIfTypeSupported:(NSString *)typeId {
    return [_supportedObjects containsObject:typeId];
}

+(SimulationObject *)createObjectOfType:(NSString *)typeId
                                 withId:(NSString *)objId {
    SimObjFactory *factory = [SimObjFactory instance];
    
    if (![factory checkIfTypeSupported:typeId]) {
        NSLog(@"Try to create object on unknown type %@", typeId);
        return nil;
    }
    
    SimulationObject *newObj = nil;
    if ([typeId isEqualToString:@"warrior"]) {
        newObj = [factory createWarrior:objId];
    } else if ([typeId isEqualToString:@"static_map"]) {
        newObj = [factory createMap:objId];
    } else if ([typeId isEqualToString:@"bullet"]) {
        newObj = [factory createBullet:objId];
    } else if ([typeId isEqualToString:@"pistol"]) {
        newObj = [factory createPistol:objId];
    } else if ([typeId isEqualToString:@"ak"]) {
        newObj = [factory createAk:objId];
    } else if ([typeId isEqualToString:@"knife"]) {
        newObj = [factory createKnife:objId];
    } else if ([typeId isEqualToString:@"bulletFraction"]) {
            newObj = [factory createBulletFraction:objId];
    } else if ([typeId isEqualToString:@"shotgun"]) {
            newObj = [factory createShotgun:objId];
    } else {
        NSLog(@"Unable to create object of type %@", typeId);
        NSAssert(false, @"Unable to create object of type %@", typeId);
    }
    //put object to register
    [factory simObjRegister][objId] = newObj;
    
    return newObj;
}

-(SimulationObject *)createBullet:(NSString*)name {
    SimulationObject *newObj = [[SimulationObject alloc]initWithId:name type:@"bullet"];
    OGLEffect *effect = [ColoredDrawEffect new];
    
    Mesh *bulletMesh = [[Mesh alloc]
        initWithBuffer:_commonBuffer numberOfElements:BULLET_ELEMCNT
        StartingFrom:(WARRIOR_ELEMCNT + MAP_ELEMCNT) WithEffect:effect drawMode:GL_TRIANGLES];
    
    MeshComposite *totalMesh = [[MeshComposite alloc] initWithMeshes:@[bulletMesh]];
    
    //[PhysicsManager gravityAcceleration] is not used for better user experiance
    newObj.physicsSt.currAcceleration = GLKVector3Make(0, -0.005, 0);

    newObj.displaySt.mesh = totalMesh;
    totalMesh.transpositionProvider = newObj.physicsSt.physObject;
    
    newObj.physicsSt.physObject.boundingBox = [[OOBB alloc] initWithWidth:-0.15 andHeight:-0.1];
    newObj.physicsSt.physObject.boundingBox.selfTransposition = GLKMatrix4MakeTranslation(0, 0.05, 0.0);
    
    [PhysicsManager addFirmObject:newObj];
    return newObj;
}

-(SimulationObject *)createBulletFraction:(NSString*)name {
    SimulationObject *newObj = [[SimulationObject alloc]initWithId:name type:@"bulletFraction"];
    OGLEffect *effect = [ColoredDrawEffect new];
    
    Mesh *bulletMesh = [[Mesh alloc]
                        initWithBuffer:_commonBuffer numberOfElements:BULLET_ELEMCNT
                        StartingFrom:(WARRIOR_ELEMCNT + MAP_ELEMCNT + BULLET_ELEMCNT + PISTOL_ELEMCNT + AK_ELEMCNT + KNIFE_ELEMCNT) WithEffect:effect drawMode:GL_TRIANGLES];
    
    MeshComposite *totalMesh = [[MeshComposite alloc] initWithMeshes:@[bulletMesh]];
    
    //[PhysicsManager gravityAcceleration] is not used for better user experiance
    newObj.physicsSt.currAcceleration = GLKVector3Make(0, -0.005, 0);
    
    newObj.displaySt.mesh = totalMesh;
    totalMesh.transpositionProvider = newObj.physicsSt.physObject;
    
    newObj.physicsSt.physObject.boundingBox = [[OOBB alloc] initWithWidth:-0.15 andHeight:-0.1];
    newObj.physicsSt.physObject.boundingBox.selfTransposition = GLKMatrix4MakeTranslation(0, 0.05, 0.0);
    
    [PhysicsManager addFirmObject:newObj];
    return newObj;
}



-(SimulationObject *)createPistol:(NSString *)name {
    SimulationObject *newObj = [[SimulationObject alloc] initWithId:name type:@"pistol"];
    
    OGLEffect *effect = [ColoredDrawEffect new];
    Mesh *pistolMesh = [[Mesh alloc]
                        initWithBuffer:_commonBuffer numberOfElements:PISTOL_ELEMCNT
                        StartingFrom:(WARRIOR_ELEMCNT + MAP_ELEMCNT + BULLET_ELEMCNT) WithEffect:effect drawMode:GL_TRIANGLES];
    MeshComposite *totalMesh = [[MeshComposite alloc] initWithMeshes:@[pistolMesh]];
    newObj.displaySt.mesh = totalMesh;
    totalMesh.transpositionProvider = newObj.physicsSt.physObject;
    
    BonePhysicsObject *weaponPO = [[BonePhysicsObject alloc] initWithName:@"Weapon"];
    weaponPO.particleEmitPoint = GLKVector3Make(0.05, 0.05, 0);
    weaponPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.35, 0.1)];
    Bone *weaponBone = [Bone new];
    weaponPO.bone = weaponBone;
    weaponBone.selfTransposition = GLKMatrix4MakeTranslation(0.0, 0.45, 0.0);
    
    pistolMesh.transpositionProvider = weaponPO;
    newObj.physicsSt.physObject = weaponPO;
    
    newObj.gameSt = [Pistol new];
    
    return newObj;
}

-(SimulationObject *)createAk:(NSString *)name {
    SimulationObject *newObj = [[SimulationObject alloc] initWithId:name type:@"ak"];
    
    OGLEffect *effect = [ColoredDrawEffect new];
    Mesh *akMesh = [[Mesh alloc]
                        initWithBuffer:_commonBuffer numberOfElements:AK_ELEMCNT
                        StartingFrom:(WARRIOR_ELEMCNT + MAP_ELEMCNT + BULLET_ELEMCNT + PISTOL_ELEMCNT) WithEffect:effect drawMode:GL_TRIANGLES];
    MeshComposite *totalMesh = [[MeshComposite alloc] initWithMeshes:@[akMesh]];
    newObj.displaySt.mesh = totalMesh;
    totalMesh.transpositionProvider = newObj.physicsSt.physObject;
    
    BonePhysicsObject *weaponPO = [[BonePhysicsObject alloc] initWithName:@"Weapon"];
    //FIXME: correct EP
    weaponPO.particleEmitPoint = GLKVector3Make(0.0, 0.1, 0);
    //FIXME: provide correct dimensions
    weaponPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.35, 0.1)];
    Bone *weaponBone = [Bone new];
    weaponPO.bone = weaponBone;
    weaponBone.selfTransposition = GLKMatrix4MakeTranslation(0.0, 0.65, 0.0);
    
    akMesh.transpositionProvider = weaponPO;
    newObj.physicsSt.physObject = weaponPO;
    
    newObj.gameSt = [AK new];
    
    return newObj;
}


-(SimulationObject *)createShotgun:(NSString *)name {
    SimulationObject *newObj = [[SimulationObject alloc] initWithId:name type:@"shotgun"];
    
    OGLEffect *effect = [ColoredDrawEffect new];
    Mesh *shotgunMesh = [[Mesh alloc]
                    initWithBuffer:_commonBuffer numberOfElements:SHOTGUN_STRIDE
                    StartingFrom:(WARRIOR_ELEMCNT + MAP_ELEMCNT + BULLET_ELEMCNT + PISTOL_ELEMCNT + AK_ELEMCNT + KNIFE_ELEMCNT + BFRACTION_ELEMCNT) WithEffect:effect drawMode:GL_TRIANGLES];
    MeshComposite *totalMesh = [[MeshComposite alloc] initWithMeshes:@[shotgunMesh]];
    newObj.displaySt.mesh = totalMesh;
    totalMesh.transpositionProvider = newObj.physicsSt.physObject;
    
    BonePhysicsObject *weaponPO = [[BonePhysicsObject alloc] initWithName:@"Weapon"];
    //FIXME: correct EP
    weaponPO.particleEmitPoint = GLKVector3Make(0.0, 0.1, 0);
    //FIXME: provide correct dimensions
    weaponPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.35, 0.1)];
    Bone *weaponBone = [Bone new];
    weaponPO.bone = weaponBone;
    weaponBone.selfTransposition = GLKMatrix4MakeTranslation(0.0, 0.65, 0.0);
    
    shotgunMesh.transpositionProvider = weaponPO;
    newObj.physicsSt.physObject = weaponPO;
    
    newObj.gameSt = [Shotgun new];
    
    return newObj;
}



-(SimulationObject *)createKnife:(NSString *)name {
    SimulationObject *newObj = [[SimulationObject alloc] initWithId:name type:@"knife"];
    
    OGLEffect *effect = [ColoredDrawEffect new];
    Mesh *knifeMesh = [[Mesh alloc]
                        initWithBuffer:_commonBuffer numberOfElements:KNIFE_ELEMCNT
                        StartingFrom:(WARRIOR_ELEMCNT + MAP_ELEMCNT + BULLET_ELEMCNT + PISTOL_ELEMCNT + AK_ELEMCNT) WithEffect:effect drawMode:GL_TRIANGLES];
    MeshComposite *totalMesh = [[MeshComposite alloc] initWithMeshes:@[knifeMesh]];
    newObj.displaySt.mesh = totalMesh;
    totalMesh.transpositionProvider = newObj.physicsSt.physObject;
    
    BonePhysicsObject *weaponPO = [[BonePhysicsObject alloc] initWithName:@"Weapon"];
    weaponPO.particleEmitPoint = GLKVector3Make(-0.05, 0.0, 0);
    //FIXME: provide correct dimensions
    weaponPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.35, 0.1)];
    Bone *weaponBone = [Bone new];
    weaponPO.bone = weaponBone;
    weaponBone.selfTransposition = GLKMatrix4MakeTranslation(0.0, 0.65, 0.0);
    
    knifeMesh.transpositionProvider = weaponPO;
    newObj.physicsSt.physObject = weaponPO;
    
    newObj.gameSt = [Knife new];
    [PhysicsManager addFirmObject:newObj];
    
    return newObj;
}


-(SimulationObject *)createMap:(NSString*) name {
    SimulationObject *newObj = [[SimulationObject alloc]initWithId:name type:@"static_map"];
    
    TextureDrawEffect *effect = [[TextureDrawEffect alloc] initWithImageName:@"Brick.png"];
    effect.rowShift = 0.5;
    
    
    Mesh *mapMesh = [[Mesh alloc]
                           initWithBuffer:_commonBuffer numberOfElements:MAP_ELEMCNT
                           StartingFrom:WARRIOR_ELEMCNT WithEffect:effect drawMode:GL_TRIANGLES];
    
    MeshComposite *totalMesh = [[MeshComposite alloc] initWithMeshes:@[mapMesh]];
    totalMesh.transpositionProvider = newObj.physicsSt.physObject;
    newObj.displaySt.mesh = totalMesh;
    
    newObj.physicsSt.physObject.boundingBox = [[CompositeCollisionBox alloc]initWithChildren:@[
        [[AxisAlignedBoundingBox alloc] initWithBounds: CGRectMake(-30.0f, -15.0f, 1.0f, 23.0f)]
    ]];
    
    int cnt = 1;
    [self addMapPartWithRectangle:CGRectMake(-3.0f, -1.0f, 6.0f, 0.5f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(-3.0f, -0.5f, 1.5f, 2.0f) andInd:&cnt];
    
    [self addMapPartWithRectangle:CGRectMake(-8.0f, -5.5f, 15.0f, 1.0f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(-8.0f, -4.5f, 2.5f, 1.5f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(7.0f, -5.5f, 2.0f, 2.0f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(9.0f, -4.5f, 0.5f, 0.3f) andInd:&cnt];
    
    [self addMapPartWithRectangle:CGRectMake(-30.0f, -14.0f, 17.0f, 6.0f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(-30.0f, -8.0f, 20.0f, 2.0f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(-13.0f, -14.0f, 5.0f, 1.5f) andInd:&cnt];
    
    [self addMapPartWithRectangle:CGRectMake(10.5f, -8.0f, 0.5f, 0.3f) andInd:&cnt];
    
    [self addMapPartWithRectangle:CGRectMake(11.0f, -12.0f, 2.0f, 7.0f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(3.0f, -14.0f, 17.0f, 2.3f) andInd:&cnt];
    
    [self addMapPartWithRectangle:CGRectMake(-5.0f, -12.0f, 5.0f, 3.0f) andInd:&cnt];
    
    [self addMapPartWithRectangle:CGRectMake(-30.0f, -15.0f, 40.0f, 1.0f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(-30.0f,  7.0f, 40.0f, 1.0f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(+30.0f, -15.0f, 1.0f, 23.0f) andInd:&cnt];
    [self addMapPartWithRectangle:CGRectMake(-30.0f, -15.0f, 1.0f, 23.0f) andInd:&cnt];
    
    [PhysicsManager addFirmObject:newObj];
    return newObj;
}

-(void)addMapPartWithRectangle:(CGRect)bb andInd:(int *)ind {
	static const int HSLICE_W = 5;
	
    float currX = bb.origin.x;
    for (int dx = HSLICE_W; dx < bb.size.width;
		 dx += HSLICE_W, currX += HSLICE_W) {
    
        NSString *mapPartId = [NSString stringWithFormat:@"static_map%d", *ind];
        SimulationObject *newObj = [[SimulationObject alloc]initWithId:mapPartId type:@"static_map"];
        newObj.displaySt.mesh = nil;
        newObj.physicsSt.physObject.boundingBox = [[AABB alloc] initWithBounds:CGRectMake(currX, bb.origin.y, HSLICE_W, bb.size.height)];
        [PhysicsManager addFirmObject:newObj];
        [self simObjRegister][mapPartId] = newObj;
        *ind += 1;
    }
    if (currX < bb.origin.x + bb.size.width) {
        NSString *mapPartId = [NSString stringWithFormat:@"static_map%d", *ind];
        SimulationObject *newObj = [[SimulationObject alloc]initWithId:mapPartId type:@"static_map"];
        newObj.displaySt.mesh = nil;
        newObj.physicsSt.physObject.boundingBox = [[AABB alloc] initWithBounds:CGRectMake(currX, bb.origin.y, bb.origin.x + bb.size.width - currX, bb.size.height)];
        [PhysicsManager addFirmObject:newObj];
        [self simObjRegister][mapPartId] = newObj;
        *ind += 1;
    }
}

-(Mesh *)nextWarriorMesh:(int *)offset
                   elems:(int)elementsCount
                  effect:(OGLEffect *)effect
               meshAccum:(NSMutableArray *)allMeshes{
    
    Mesh *mesh = [[Mesh alloc]
                  initWithBuffer:_commonBuffer numberOfElements:elementsCount
                  StartingFrom:*offset WithEffect:effect drawMode:GL_TRIANGLES];
    *offset += elementsCount;
    [allMeshes addObject:mesh];
    return mesh;
}

+(NSDictionary *)warriorInitLegJointsCfg {
    return @{hsLeftKneeId:@(M_PI/7), hsLeftHipId:@(-M_PI/7), hsLeftAnkleId:@(0),
             hsRightKneeId:@(M_PI/10), hsRightHipId:@(-M_PI/10), hsRightAnkleId:@(0),
             hsLoinId:@(0)
             };
}

-(SimulationObject *)createWarrior:(NSString *)name {
    SimulationObject *newObj = [[SimulationObject alloc] initWithId:name type:@"warrior"];
    
    ////// Setup warrior bounding boxes
    NSMutableArray *warriorPhysicsObjects = [NSMutableArray new];
    NSMutableArray *warriorJoints = [NSMutableArray new];
    //// Head
    
    BonePhysicsObject *headPO = [[BonePhysicsObject alloc] initWithName:@"Head"];
    headPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.25, 0.3)];
    Bone *headBone = [Bone new];
    headBone.selfTransposition = GLKMatrix4MakeTranslation(0.02, 0.6, 0);
    headPO.bone = headBone;
    [warriorPhysicsObjects addObject:headPO];
    
    //// Body
    // Torso
    BonePhysicsObject *torsoPO = [[BonePhysicsObject alloc] initWithName:@"Torsp"];
    torsoPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.30, 0.60)];
    Bone *torsoBone = [Bone new];
    torsoPO.bone = torsoBone;
    torsoBone.selfTransposition = GLKMatrix4MakeTranslation(0.05, 0.1, 0);
    Joint *neckJoint = [[Joint alloc] initWithName:hsNeckId parentBone:torsoBone
                                               childBone:headBone connPoint:GLKVector3Make(0.15, 0.0, 0.0)];
    [neckJoint setRotationConstrMinAng:-M_PI/8  maxRotAngle:M_PI/8 isFwrd:YES];
    [warriorJoints addObject:neckJoint];
    [torsoBone.childJoints addObject:neckJoint];
    [warriorPhysicsObjects addObject:torsoPO];
    
    // Hips
    BonePhysicsObject *hipsPO = [[BonePhysicsObject alloc] initWithName:@"Hips"];
    hipsPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.4, 0.1)];
    Bone *hipsBone = [Bone new];
    hipsPO.bone = hipsBone;
    Joint *loinJoint = [[Joint alloc ]initWithName:hsLoinId parentBone:hipsBone
                     childBone:torsoBone connPoint:GLKVector3Make(0.15, 0.05, 0.0)];
    [loinJoint setRotationConstrMinAng:-M_PI/7 maxRotAngle:M_PI/7 isFwrd:YES];
    [warriorJoints addObject:loinJoint];
    [hipsBone.childJoints addObject:loinJoint];
    [warriorPhysicsObjects addObject:hipsPO];
    
    //* Left Leg
    BonePhysicsObject *leftLegHiPO = [[BonePhysicsObject alloc] initWithName:@"leftLegHi"];
    leftLegHiPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.2, 0.3)];
    Bone *leftLegHiBone = [Bone new];
    leftLegHiPO.bone = leftLegHiBone;
    leftLegHiBone.selfTransposition = GLKMatrix4MakeTranslation(-0.05, -0.25, 0);
    Joint *hipsLLHJoint = [[Joint alloc ]initWithName:hsLeftHipId parentBone:hipsBone
                                            childBone:leftLegHiBone connPoint:GLKVector3Make(0.1, 0.25, 0.0)];
    [hipsLLHJoint setRotationConstrMinAng:(-M_PI * 3 / 5) maxRotAngle:M_PI_4 isFwrd:YES];
    [hipsLLHJoint rotateChildOverJoint:-M_PI/7];
    [warriorJoints addObject:hipsLLHJoint];
    [hipsBone.childJoints addObject:hipsLLHJoint];
    [warriorPhysicsObjects addObject:leftLegHiPO];
    
    BonePhysicsObject *leftLegLoPO = [[BonePhysicsObject alloc] initWithName:@"LeftLegLo"];
    leftLegLoPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.15, 0.25)];
    Bone *leftLegLoBone = [Bone new];
    leftLegLoPO.bone = leftLegLoBone;
    leftLegLoBone.selfTransposition = GLKMatrix4MakeTranslation(0.03, -0.22, 0);
    Joint *leftKneeJoint = [[Joint alloc ]initWithName:hsLeftKneeId parentBone:leftLegHiBone
                                           childBone:leftLegLoBone connPoint:GLKVector3Make(0.07, 0.2, 0.0)];
    [leftKneeJoint setRotationConstrMinAng:0 maxRotAngle:M_PI * 0.7 isFwrd:YES];
    [leftKneeJoint rotateChildOverJoint:M_PI/7];
    [warriorJoints addObject:leftKneeJoint];
    [leftLegHiBone.childJoints addObject:leftKneeJoint];
    [warriorPhysicsObjects addObject:leftLegLoPO];
    
    
    BonePhysicsObject *leftLegShPO = [[BonePhysicsObject alloc] initWithName:@"LeftLegSh"];
    leftLegShPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.3, 0.2)];
    Bone *leftLegShBone = [Bone new];
    leftLegShPO.bone = leftLegShBone;
    leftLegShBone.selfTransposition = GLKMatrix4MakeTranslation(-0.12, -0.20, 0);
    Joint *leftAnkleJoint = [[Joint alloc ]initWithName:hsLeftAnkleId parentBone:leftLegLoBone
                                            childBone:leftLegShBone connPoint:GLKVector3Make(0.2, 0.2, 0.0)];
    [leftAnkleJoint setRotationConstrMinAng:-M_PI/6 maxRotAngle:M_PI/6 isFwrd:YES];
    [warriorJoints addObject:leftAnkleJoint];
    [leftLegLoBone.childJoints addObject:leftAnkleJoint];
    [warriorPhysicsObjects addObject:leftLegShPO];
    
    
    //* Right Leg
    BonePhysicsObject *rightLegHiPO = [[BonePhysicsObject alloc] initWithName:@"RightLegHi"];
    rightLegHiPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.2, 0.3)];
    Bone *rightLegHiBone = [Bone new];
    rightLegHiPO.bone = rightLegHiBone;
    rightLegHiBone.selfTransposition = GLKMatrix4MakeTranslation(0.20, -0.25, 0);
    Joint *hipsRLHJoint = [[Joint alloc ]initWithName:hsRightHipId parentBone:hipsBone
                                              childBone:rightLegHiBone connPoint:GLKVector3Make(0.1, 0.25, 0.0)];
    [hipsRLHJoint setRotationConstrMinAng:-M_PI * 3 / 5 maxRotAngle:M_PI_4 isFwrd:YES];
    [hipsRLHJoint rotateChildOverJoint:M_PI/7];
    [warriorJoints addObject:hipsRLHJoint];
    [hipsBone.childJoints addObject:hipsRLHJoint];
    [warriorPhysicsObjects addObject:rightLegHiPO];
    
    
    BonePhysicsObject *rightLegLoPO = [[BonePhysicsObject alloc] initWithName:@"RightLegLo"];
    rightLegLoPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.15, 0.25)];
    Bone *rightLegLoBone = [Bone new];
    rightLegLoPO.bone = rightLegLoBone;
    rightLegLoBone.selfTransposition = GLKMatrix4MakeTranslation(0.03, -0.22, 0);
    Joint *rightKneeJoint = [[Joint alloc ]initWithName:hsRightKneeId parentBone:rightLegHiBone
                                            childBone:rightLegLoBone connPoint:GLKVector3Make(0.07, 0.2, 0.0)];
    [rightKneeJoint setRotationConstrMinAng:-M_PI/8 maxRotAngle:M_PI * 0.7 isFwrd:YES];
    [rightKneeJoint rotateChildOverJoint:-M_PI/8];
    [warriorJoints addObject:rightKneeJoint];
    [rightLegHiBone.childJoints addObject:rightKneeJoint];
    [warriorPhysicsObjects addObject:rightLegLoPO];
    
    
    BonePhysicsObject *rightLegShPO = [[BonePhysicsObject alloc] initWithName:@"RightLegSh"];
    rightLegShPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.3, 0.2)];
    Bone *rightLegShBone = [Bone new];
    rightLegShPO.bone = rightLegShBone;
    rightLegShBone.selfTransposition = GLKMatrix4MakeTranslation(-0.12, -0.20, 0);
    Joint *rightAnkleJoint = [[Joint alloc ]initWithName:hsRightAnkleId parentBone:rightLegLoBone
                                           childBone:rightLegShBone connPoint:GLKVector3Make(0.2, 0.2, 0.0)];
    [rightAnkleJoint setRotationConstrMinAng:-M_PI/6 maxRotAngle:M_PI/6 isFwrd:YES];
    [warriorJoints addObject:rightAnkleJoint];
    [rightLegLoBone.childJoints addObject:rightAnkleJoint];
    [warriorPhysicsObjects addObject:rightLegShPO];

    
    
    //* Left Arm
    BonePhysicsObject *leftArmHiPO = [[BonePhysicsObject alloc] initWithName:@"LeftArmHi"];
    leftArmHiPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.2, 0.45)];
    Bone *leftArmHiBone = [Bone new];
    leftArmHiPO.bone = leftArmHiBone;
    leftArmHiBone.selfTransposition = GLKMatrix4MakeTranslation(-0.02, 0.13, 0.0);
    Joint *leftShoulderJoint = [[Joint alloc] initWithName:hsLeftShoulderId
                                            parentBone:torsoBone childBone:leftArmHiBone
                                             connPoint:GLKVector3Make(0.07, 0.42, 0.0)];
    [leftShoulderJoint setRotationConstrMinAng:-M_PI maxRotAngle:M_PI_2 isFwrd:YES];
    [warriorJoints addObject:leftShoulderJoint];
    [torsoBone.childJoints addObject:leftShoulderJoint];
    [warriorPhysicsObjects addObject:leftArmHiPO];

    
    BonePhysicsObject *leftArmLoPO = [[BonePhysicsObject alloc] initWithName:@"LeftArmLo"];
    leftArmLoPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.12, 0.35)];
    Bone *leftArmLoBone = [Bone new];
    leftArmLoPO.bone = leftArmLoBone;
    leftArmLoBone.selfTransposition = GLKMatrix4MakeTranslation(0.0, -0.3, 0.0);
    Joint *leftElbowJoint = [[Joint alloc] initWithName:hsLeftElbowId
                                          parentBone:leftArmHiBone childBone:leftArmLoBone
                                           connPoint:GLKVector3Make(0.07, 0.4, 0.0)];
    [leftElbowJoint setRotationConstrMinAng:-M_PI * 7/8 maxRotAngle:0 isFwrd:YES];
    [warriorJoints addObject:leftElbowJoint];
    [leftArmHiBone.childJoints addObject:leftElbowJoint];
    [warriorPhysicsObjects addObject:leftArmLoPO];
    
    //* Right Arm
    BonePhysicsObject *rightArmHiPO = [[BonePhysicsObject alloc] initWithName:@"RightArmHi"];
    rightArmHiPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.2, 0.45)];
    Bone *rightArmHiBone = [Bone new];
    rightArmHiPO.bone = rightArmHiBone;
    rightArmHiBone.selfTransposition = GLKMatrix4MakeTranslation(0.1, 0.15, 0.0);
    Joint *rightShoulderJoint = [[Joint alloc] initWithName:hsRightShoulderId
        parentBone:torsoBone childBone:rightArmHiBone
        connPoint:GLKVector3Make(0.07, 0.42, 0.0)];

    [rightShoulderJoint setRotationConstrMinAng:-M_PI maxRotAngle:M_PI_2 isFwrd:YES];
    [warriorJoints addObject:rightShoulderJoint];
    [torsoBone.childJoints addObject:rightShoulderJoint];
    [warriorPhysicsObjects addObject:rightArmHiPO];

    
    BonePhysicsObject *rightArmLoPO = [[BonePhysicsObject alloc] initWithName:@"RightArmLo"];
    rightArmLoPO.boundingBox = [[OOBB alloc] initWithBounds:CGRectMake(0.0f, 0.0f, 0.12, 0.45)];
    Bone *rightArmLoBone = [Bone new];
    rightArmLoPO.bone = rightArmLoBone;
    rightArmLoBone.selfTransposition = GLKMatrix4MakeTranslation(0.05, 0.05, 0.0);
    Joint *rightElbowJoint = [[Joint alloc] initWithName:hsRightElbowId
        parentBone:rightArmHiBone childBone:rightArmLoBone
        connPoint:GLKVector3Make(0.07, 0.0, 0.0)];
    [rightElbowJoint setRotationConstrMinAng:M_PI/8 maxRotAngle:M_PI isFwrd:YES];
    [warriorJoints addObject:rightElbowJoint];
    [rightArmHiBone.childJoints addObject:rightElbowJoint];
    [rightElbowJoint rotateChildOverJoint:M_PI_2];
    [warriorPhysicsObjects addObject:rightArmLoPO];
    
    //rightArmWithWeaponPO.selfTranslation = GLKMatrix4MakeTranslation(0.0f, 0.45f, 0.0f);
    SkeletonPhysicsObject *warriorPO = [[SkeletonPhysicsObject alloc] initWithInnerParts:warriorPhysicsObjects controllerObject:hipsPO joints:warriorJoints];
    warriorPO.name = name;
    
    ////// Setup meshes
    OGLEffect *effect = [ColoredDrawEffect new];
    
    NSMutableArray *allMeshes = [NSMutableArray new];
    int meshOffset = 0;
    
    Mesh *torsoMesh = [self nextWarriorMesh:&meshOffset elems:(MESH_RECT_ELEMS + 2 * MESH_TRAPEZ_ELEMS)effect:effect meshAccum:allMeshes];
    torsoMesh.transpositionProvider = torsoPO;

    Mesh *headMesh = [self nextWarriorMesh:&meshOffset elems:(MESH_RECT_ELEMS * 4 + 1 * 3 + MESH_TRAPEZ_ELEMS * 4) effect:effect meshAccum:allMeshes];
    headMesh.transpositionProvider = headPO;
    
    Mesh *hipsMesh = [self nextWarriorMesh:&meshOffset elems:MESH_TRAPEZ_ELEMS effect:effect meshAccum:allMeshes];
    hipsMesh.transpositionProvider = hipsPO;
    
    
    Mesh *leftArmHighMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    leftArmHighMesh.transpositionProvider = leftArmHiPO;
    
    Mesh *leftArmLowMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    leftArmLowMesh.transpositionProvider = leftArmLoPO;

    Mesh *leftLegHighMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    leftLegHighMesh.transpositionProvider = leftLegHiPO;
    
    Mesh *leftLegLowMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    leftLegLowMesh.transpositionProvider = leftLegLoPO;
    
    Mesh *leftShoeMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    leftShoeMesh.transpositionProvider = leftLegShPO;
    
    Mesh *rightLegHighMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    rightLegHighMesh.transpositionProvider = rightLegHiPO;
    Mesh *rightLegLowMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    rightLegLowMesh.transpositionProvider = rightLegLoPO;
    Mesh *rightShoeMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    rightShoeMesh.transpositionProvider = rightLegShPO;
   
    //
    Mesh *rightArmHighMesh = [self nextWarriorMesh:&meshOffset elems:(MESH_RECT_ELEMS + 2 * MESH_TRAPEZ_ELEMS) effect:effect meshAccum:allMeshes];
    rightArmHighMesh.transpositionProvider = rightArmHiPO;
    Mesh *rightArmLowMesh = [self nextWarriorMesh:&meshOffset elems:MESH_RECT_ELEMS effect:effect meshAccum:allMeshes];
    rightArmLowMesh.transpositionProvider = rightArmLoPO;
     
    newObj.displaySt.mesh = [[MeshComposite alloc] initWithMeshes:allMeshes];
    
    newObj.physicsSt.physObject = warriorPO;
    
    newObj.physicsSt.physObject.boundingBox = [[AABB alloc] initWithBounds:CGRectMake(-0.4f, -0.67f, 1.0f, 1.8f)];
    newObj.physicsSt.physObject.boundingBox.transpositionProvider = hipsPO;
    
    newObj.physicsSt.currAcceleration = [PhysicsManager gravityAcceleration];
    newObj.physicsSt.ttl = INFINITY;

    [PhysicsManager addAlwaysMovingObject:name];
    [PhysicsManager addFirmObject:newObj];
    
    [newObj setParticleEmitterCorr:@{ @"bullet":@"Weapon", @"knife":@"RightArmLo", @"bulletFraction":@"Weapon"}];
    
    newObj.gameSt = [[WarriorGameStrategy alloc] initWithObjId:name andType:@"warrior"];
    ((WGS*)newObj.gameSt).hitPoints = 8;
    
    return newObj;
}


@end
