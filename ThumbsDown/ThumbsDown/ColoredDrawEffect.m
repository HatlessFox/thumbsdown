
//
//  ColoredDrawEffect.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/18/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "ColoredDrawEffect.h"
#import "WorldCamera.h"
#import "SimulationObject.h"
#import "DrawingCtx.h"
#import "SimObjFactory.h"

@implementation ColoredDrawEffect {
    NSDictionary *_attributes;
}

- (id)init {
    _opacity = 1.0;
    _attributes = @{@"a_position":@1, @"a_color":@2};
    return [super initWithShader:@"ColoredOutput"
                   andAttributes:_attributes];
}

- (id)initWithOpacity:(float)opacity {
    self = [self init];
    if (self) {
        _opacity = opacity;
    }
    return self;
}

- (void)bindAttributes:(id<OGLBuffer>)buffer {
    [buffer makeActive];
    [buffer enableAttributeSupport:1
                            ofType:GL_FLOAT withComponentsCount:3
                          byOffset:0];
    [buffer enableAttributeSupport:2
                            ofType:GL_FLOAT withComponentsCount:3 byOffset:12];
}

- (void)configureUniforms:(GLKMatrix4)meshTransform {
    static GLint viewProjMatrUniform[2] = {-1, -1};
    if (viewProjMatrUniform[0] == -1) {
        [self extractUniformLocations:@[@"u_viewProjectionMatrix", @"u_opacity"]
                            toStorage:viewProjMatrUniform];
    }
    
    GLKMatrix4 mvpM = meshTransform;
    WorldCamera *cam = [WorldCamera instance];
    mvpM = GLKMatrix4Multiply(cam.viewMatrix, mvpM);
   /* SimulationObject *so = [[SimObjFactory instance]createdObjectById:[DrawingCtx instance].currDrawObjectId ];
    mvpM = GLKMatrix4Multiply(mvpM,so.camTransformMtx);
    */
    
    mvpM = GLKMatrix4Multiply(cam.projectionMatrix, mvpM);
    
    glUniformMatrix4fv(viewProjMatrUniform[0], 1, 0, mvpM.m);
    glUniform1f(viewProjMatrUniform[1], _opacity);
}

@end
