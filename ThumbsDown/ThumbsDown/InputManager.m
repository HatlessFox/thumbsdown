//
//  InputManager.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "InputManager.h"
#import "WorldCamera.h"
#import "SimObjFactory.h"
#import "MeshComposite.h"
#import "PhysicsManager.h"
#import "SimulationInfo.h"
#import "GameManager.h"
#import "MoveCommand.h"
#import "SkeletonMovementCommand.h"
#import "SkeletonPhysicsObject.h"
#import "AnimationManager.h"
#import "SkeletonKeyFrame.h"
#import "Animation.h"
#import "HumanSkeletonConstants.h"
#import "WarriorGameStrategy.h"

@interface InputManager()
@property (readonly, nonatomic) NSMutableArray *commands;
@property (weak, nonatomic) UIView *mainView;
@property (assign, nonatomic) CGPoint targetLocation;
@property (assign, nonatomic) BOOL targetLocationIsUpToDate;


@property (strong, nonatomic) UIPinchGestureRecognizer *pinch;
@property (strong, nonatomic) NSTimer *timer;


@property (assign, nonatomic) int wpnIndex;

@end

@implementation InputManager

-(id)init {
    self = [super init];
    if (self) {
        _commands = [NSMutableArray new];
        _targetLocationIsUpToDate = YES;
    }
    return self;
}

+(InputManager*)instance {
    static InputManager *inst;
    if (!inst) {
        inst = [InputManager new];
    }
    return inst;
}

+(void)setupGestureInputs:(UIView *)mainView {
    [InputManager instance].mainView = mainView;
    

    //handle pinch
    [InputManager instance].pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:[InputManager class] action:@selector(handlePinch:)];
    [mainView addGestureRecognizer:[InputManager instance].pinch];
    
    //tap
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:[InputManager class] action:@selector(handleTap:)];
    [mainView addGestureRecognizer:tap];
}

+(void)rotateArmOf:(SimulationObject *)warrior byDegree:(float)degree {
    NSMutableDictionary *jointRotations = [NSMutableDictionary new];
    jointRotations[hsRightShoulderId] = @(GLKMathDegreesToRadians(degree));
    if ([InputManager instance].aimOn) {
        jointRotations[hsLeftShoulderId] = @(GLKMathDegreesToRadians(degree));
    }
    
    id<Command> skelCmd = [[SkeletonMovementCommand alloc] initWithActorName:[SimulationInfo controlledPlayerName] commands:jointRotations];
    NSLog(@"Rotation: %f", degree);
    [[InputManager instance].commands addObject:skelCmd];
}

+(GLKVector4)screenCoordToWorld:(CGPoint)screenCoord {
    UIView *mainView = [InputManager instance].mainView;
    
    GLKVector4 tmpV = GLKVector4Make(
        2.0 * screenCoord.x / mainView.bounds.size.width - 1,
        -2.0 * screenCoord.y / mainView.bounds.size.height + 1 , 0.0f, 1.0f);
    
    //Inverse camera transformations
    WorldCamera *cam = [WorldCamera instance];
    GLKMatrix4 camTransform = GLKMatrix4Multiply(cam.projectionMatrix, cam.viewMatrix);
    bool isInv = true;
    GLKMatrix4 camInv = GLKMatrix4Invert(camTransform, &isInv);
    return GLKMatrix4MultiplyVector4(camInv, tmpV);
}

+(void)handlePinch:(UIPinchGestureRecognizer *)sender {
    if (sender.state != UIGestureRecognizerStateEnded) { return;}
    [WorldCamera instance].zoomFactor /= [sender scale];
}

+(void)handleTap:(UITapGestureRecognizer *)sender {
    if (sender.state != UIGestureRecognizerStateEnded) { return;}
    CGPoint tapLocation = [sender locationInView:[InputManager instance].mainView];
    GLKVector4 worldCoord = [self screenCoordToWorld:tapLocation];
    
    SimulationObject *tappedObject = [PhysicsManager lookupFirmObjectByLocation:worldCoord];
    if ([tappedObject.type isEqualToString:@"warrior"]) {
        [SimulationInfo setControlledPlayerName:tappedObject.name];
    }
}

+(void)changeWeapon {
    InputManager *im = [InputManager instance];
    im.wpnIndex = (im.wpnIndex + 1) % 4;
    NSString *wpnType;
    switch (im.wpnIndex) {
        case 0:
            wpnType = @"knife";
            break;
        case 1:
            wpnType = @"pistol";
            break;
        case 2:
            wpnType = @"shotgun";
            break;
        case 3:
            wpnType = @"ak";
            break;
            
        default:
            break;
    }
    [((WGS *)[SimulationInfo player].gameSt) addWeapon:wpnType];
}

+(void)reverseAimMode {
    InputManager *im = [InputManager instance];
    im.aimOn = !im.aimOn;
    
    if (im.aimOn == YES) {
        //remove gesture recognizers
        for (UIGestureRecognizer *gr in im.mainView.gestureRecognizers) {
            [im.mainView removeGestureRecognizer:gr];
        }
        [AnimationManager addAnimationId:@"WarriorAimOn" byKey:@"PlayerAimimg"
                               forTarget:[SimulationInfo controlledPlayerName]];
        [im.mainView removeGestureRecognizer:im.pinch];        
    } else {
        [self setupGestureInputs:im.mainView];
        [AnimationManager addAnimationId:@"WarriorAimOff" byKey:@"PlayerAimimg"
                               forTarget:[SimulationInfo controlledPlayerName]];
        [im.mainView addGestureRecognizer:im.pinch];
    }
}

+(void)addMovement:(NSTimer *)timer {
    float dir = [timer.userInfo[@"direction"] floatValue];
    PhysicsObject *po = [SimulationInfo player].physicsSt.physObject;
    GLKVector3 sight = GLKMatrix4MultiplyVector3(po.boundingBox.selfTransposition, GLKVector3Make(-1, 0, 0));
    float sightDir = GLKVector3CrossProduct(sight, GLKVector3Make(0, 1, 0)).z > 0 ? 1 : -1;
    
    if (dir * sightDir > 0) {
        //FWD direction
        SkeletonKeyFrame *frame1 = [[SkeletonKeyFrame alloc] initWithDuration:0.15 jointRotation:@{hsLeftKneeId: @(0), hsLeftHipId:@(0), hsLeftAnkleId:@(0), hsRightHipId:@(M_PI / 6), hsRightAnkleId:@(-M_PI / 8),hsRightKneeId:@(M_PI / 8), hsLoinId:@(0)} transposition:GLKVector3Make(dir*0.35, 0.0, 0) ignoreChecks:YES];
        SkeletonKeyFrame *frame2 = [[SkeletonKeyFrame alloc] initWithDuration:0.15 jointRotation:@{hsRightHipId:@(-M_PI/6), hsRightKneeId:@(M_PI/4),hsRightAnkleId:@(0), hsLeftHipId:@(M_PI/4), hsLeftKneeId:@(0), hsLeftAnkleId:@(0)} transposition:GLKVector3Make(dir*0.35, 0.0, 0) ignoreChecks:YES];
        SkeletonKeyFrame *frame3 = [[SkeletonKeyFrame alloc] initWithDuration:0.1 jointRotation:[SimObjFactory warriorInitLegJointsCfg] transposition:GLKVector3Make(dir*0.3, 0.0, 0) ignoreChecks:YES];

        Animation *anim = [[Animation alloc] initWithKeyFrames:@[frame1, frame2, frame3]];
        [anim setTargetId:[SimulationInfo controlledPlayerName]];
    
        [AnimationManager addAnimation:anim byKey:@"warrior" withMerge:NO];
    } else {
        //BWD step
        SkeletonKeyFrame *frame1 = [[SkeletonKeyFrame alloc] initWithDuration:0.1
            jointRotation:@{
                hsLeftKneeId: @(0), hsLeftHipId:@(0), hsLeftAnkleId:@(0),
                hsRightHipId:@(-M_PI/6), hsRightKneeId:@(M_PI/6), hsRightAnkleId:@(0),
                hsLoinId:@(0)} transposition:GLKVector3Make(dir*0.1, 0.0, 0) ignoreChecks:YES];
        SkeletonKeyFrame *frame2 = [[SkeletonKeyFrame alloc] initWithDuration:0.1 jointRotation:[SimObjFactory warriorInitLegJointsCfg] transposition:GLKVector3Make(dir*0.1, 0.0, 0) ignoreChecks:YES];
        
        Animation *anim = [[Animation alloc] initWithKeyFrames:@[frame1, frame2]];
        [anim setTargetId:[SimulationInfo controlledPlayerName]];
        
        [AnimationManager addAnimation:anim byKey:@"warrior" withMerge:NO];
    }
}

+(void)aim:(UITouch *)touch {
    [InputManager instance].targetLocation = [touch locationInView:[InputManager instance].mainView];
    [InputManager instance].targetLocationIsUpToDate = NO;
}

+(void)aimTo:(CGPoint)targetLocation {
    GLKVector4 worldCoord = [self screenCoordToWorld:targetLocation];
    GLKVector3 worldCoordPoint = GLKVector3Make(worldCoord.x, worldCoord.y, worldCoord.z);
    
    SimulationObject *player = [SimulationInfo player];
    SkeletonPhysicsObject *physObj = (SkeletonPhysicsObject *)player.physicsSt.physObject;
    
    //FIXME: get emit point from current weapon
    GLKVector3 emitPoint = [physObj physicsPartByName:@"Weapon"].particleEmitPoint;
    Joint *weaponArmJoint = [physObj jointById:hsRightWristId];
    GLKVector3 basePoint = weaponArmJoint.pointOfConnection;
    //trick to have current barrel direction correct
    basePoint.y = emitPoint.y;
    
    GLKMatrix4  mostUpToDateTranslation = [weaponArmJoint.childBone compoundTranslation];
    
    GLKVector3 baseWorldCoords = GLKMatrix4MultiplyVector3WithTranslation(mostUpToDateTranslation, basePoint);
    
    GLKVector3 desiredDirection = GLKVector3Subtract(worldCoordPoint, baseWorldCoords);
    
    GLKVector3 currDirection = GLKVector3Subtract(GLKMatrix4MultiplyVector3WithTranslation(mostUpToDateTranslation, emitPoint), baseWorldCoords);
    
    //compute angle between desired and current using cosine thm
    float dotProd = desiredDirection.x * currDirection.x + desiredDirection.y * currDirection.y;
    
    currDirection.z = desiredDirection.z = 0;
    float lenProd = GLKVector3Length(desiredDirection) * GLKVector3Length(currDirection);
    float cosAngle = dotProd / lenProd;
    if (cosAngle > 1) { cosAngle = 1; }
    else if (cosAngle < -1) { cosAngle = -1; }
    
    
    float angle = GLKMathRadiansToDegrees(acosf(cosAngle));
    if (GLKVector3CrossProduct(currDirection, desiredDirection).z < 0) { angle *= -1; }
    
    //check if model rotated over Y
    //FIXME: remove somehow assumption that rotated matrix has small value less than zero as sin theta e.g. intro quaternions
    if (mostUpToDateTranslation.m20 < 0.0) { angle *= -1; }
    
    [self rotateArmOf:player byDegree:angle];
}

+(void)disableMovement {
    InputManager *im = [InputManager instance];
    [im.timer invalidate];
    im.timer = nil;
    
    if (![im.mainView.gestureRecognizers containsObject:im.pinch]) {
        [im.mainView addGestureRecognizer:im.pinch];
    }
    /*
    SkeletonKeyFrame *frame = [[SkeletonKeyFrame alloc] initWithDuration:0.1 jointRotation:[SimObjFactory warriorInitLegJointsCfg]  transposition:GLKVector3Make(0.0, 0.0, 0)];
    Animation *anim = [[Animation alloc] initWithKeyFrames:@[frame]];
    [anim setTargetId:[SimulationInfo controlledPlayerName]];
    
    [AnimationManager addAnimation:anim byKey:@"warrior" withMerge:YES];
     */
}

+(void)enableMovement:(float)dir {
    InputManager *im = [InputManager instance];
    im.timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                target:self
                                              selector:@selector(addMovement:)
                                              userInfo:@{@"direction":@(dir)}
                                               repeats:YES];
    [im.mainView removeGestureRecognizer:im.pinch];
}

+(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    InputManager *im = [InputManager instance];
    
    NSMutableSet *mainViewTouches = [NSMutableSet new];
    for (UITouch *t in [event allTouches]) {
        if (t.view == [self instance].mainView || !t.view) {
            [mainViewTouches addObject:t];
        }
    }
    UITouch *touch = [mainViewTouches anyObject];
    if (touch == nil) { return; }
    if ([self instance].aimOn) { return [self aim:touch]; }
    
    GLKVector3 compoundDisplacement = GLKVector3Make(0, 0, 0);
    for (UITouch *touch in mainViewTouches) {
        CGPoint tapLocation = [touch locationInView:[InputManager instance].mainView];
        CGPoint prevLoc = [touch previousLocationInView:[InputManager instance].mainView];
        compoundDisplacement = GLKVector3Add(compoundDisplacement, GLKVector3Make(tapLocation.x - prevLoc.x, tapLocation.y-prevLoc.y, 0));
    }
    
    switch (mainViewTouches.count) {
        case 1:
        case 2: {
            if (fabs(compoundDisplacement.x) >= 7) {
            
                float reqDir = compoundDisplacement.x < 0 ? -1 : 1;
                if (![InputManager instance].timer) {
                    [self enableMovement:reqDir];
                } else {
                    float dir = [im.timer.userInfo[@"direction"] floatValue];
                    if (dir != reqDir && fabs(compoundDisplacement.x/ mainViewTouches.count) > 7) {
                        [self disableMovement];
                        [self enableMovement:reqDir];
                    }
                }
            }
            
            if (fabs(compoundDisplacement.y) > 3) {
                [self rotateArmOf:[SimulationInfo player] byDegree:1 * (compoundDisplacement.y > 0 ? 1 : -1)];
            }
            

        }
        default:
            break;
    }
}

+(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    InputManager *im = [InputManager instance];
    BOOL mainViewHasTouches = NO;
    for (UITouch *touch in [event allTouches]) {
        if (touch.view == im.mainView && touch.phase != UITouchPhaseEnded) {
            mainViewHasTouches = YES;
        }
    }
    if (!mainViewHasTouches) {
       [InputManager disableMovement];
    }
}

+(void)recoil:(NSString *)playerId {
    InputManager *im = [InputManager instance];
    NSString *animationId = im.aimOn ? @"WeaponRecoilAiming" : @"WeaponRecoil";
    [AnimationManager addAnimationId:animationId byKey:@"weaponRecoil" forTarget:playerId];
    
    /*if (!im.aimOn) {
        float recoilFactor = im.aimOn ? 2 : 1;
        id<Command> recoilCmd = [[SkeletonMovementCommand alloc] initWithActorName:[SimulationInfo controlledPlayerName] commands:@{hsRightWristId:@(M_PI/30 - (recoilFactor * M_PI / 15 * rand() / RAND_MAX))}];
        [InputManager submitCommand:recoilCmd];
    }*/
    
}

+(void)performCommands {
    InputManager *im = [InputManager instance];
    
    if (!im.targetLocationIsUpToDate) {
        [self aimTo:im.targetLocation];
        im.targetLocationIsUpToDate = YES;
    }
    for (id<Command> cmd in im.commands) {
        [cmd performCommand];
    }
    [im.commands removeAllObjects];
}

+(void)submitCommand:(id<Command>)cmd {
    InputManager *im = [InputManager instance];
    [im.commands addObject:cmd];
}

@end
