//
//  ShaderUtils.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/16/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "EffectUtils.h"
@import GLKit;

const unsigned BYTES_PER_RGBA = 4;

@implementation OGLKTextureInfo

-(id)initWithBufferId:(GLuint)bufferId type:(GLenum)type
                width:(GLuint)width height:(GLuint)height {
    self = [super init];
    if (self) {
        _bufferId = bufferId;
        _type = type;
        _width = width;
        _height = height;
    }
    return self;
}

-(void)dealloc {
    if (_bufferId) {
        glDeleteTextures(1, &_bufferId);
        _bufferId = 0;
    }
}

@end


@implementation EffectUtils {
    NSMutableDictionary *_compiledShadersCache;
}

-(id)init {
    self = [super init];
    if (self) {
        _compiledShadersCache = [NSMutableDictionary new];
    }
    return self;
}

+ (EffectUtils *)instance {
    static EffectUtils *inst;
    if (!inst) { inst = [EffectUtils new]; }
    return inst;
}

#pragma mark - Shader Utils

-(NSMutableDictionary *)compShCache { return _compiledShadersCache; }

+ (void)addToCache:(NSString *)resourceName value:(GLuint)value {
    [[self instance] compShCache][resourceName] = @(value);
}

+ (GLuint)getFromCache:(NSString *)resourceName {
    id value = [[self instance] compShCache][resourceName];
    if (value == nil) { return 0; }
    return [(NSNumber *)value intValue];
}


+ (GLuint)loadShader:(NSString *)shaderResourceName
      WithAttributes:(NSDictionary *)attrs {
    GLuint programId = glCreateProgram();
    if (!programId) {
        NSLog(@"Unable to create program for shader %@",
              shaderResourceName);
        return 0;
    }
    
    //compile shaders
    GLuint vertShId = [EffectUtils compileShader:shaderResourceName
                                          ofType:GL_VERTEX_SHADER];
    if (!vertShId) { return 0;}
    GLuint fragShId = [EffectUtils compileShader:shaderResourceName
                                          ofType:GL_FRAGMENT_SHADER];
    if (!fragShId) {
        glDeleteShader(vertShId);
        return 0;
    }
    
    //prepare for linking
    glAttachShader(programId, vertShId);
    glAttachShader(programId, fragShId);
    
    for (NSString *attrName in attrs) {
        glBindAttribLocation(programId, [attrs[attrName] integerValue], [attrName UTF8String]);
    }
    
    if (![EffectUtils linkProgram:programId]) {
        NSLog(@"Failed to link program for shader %@", shaderResourceName);
       // glDeleteShader(vertShId);
       // glDeleteShader(fragShId);
        glDeleteProgram(programId);
        return 0;
    }
    
    glDetachShader(programId, vertShId);
    glDetachShader(programId, fragShId);
    //glDeleteShader(vertShId);
    //glDeleteShader(fragShId);
    
    return programId;
}

+ (GLuint)compileShader:(NSString *)shaderName ofType:(GLenum)type {
    NSString *shaderFilePathName = [[NSBundle mainBundle]
        pathForResource:shaderName
                 ofType:type == GL_VERTEX_SHADER ? @"vsh" : @"fsh"];
    
    GLuint shaderId = [self getFromCache:shaderFilePathName];
    if (shaderId) {
        return shaderId;
    }
    
    const GLchar *shaderSource = (GLchar *)
        [[NSString stringWithContentsOfFile:shaderFilePathName
                                   encoding:NSUTF8StringEncoding
                                      error:nil] UTF8String];
    shaderId = glCreateShader(type);
    glShaderSource(shaderId, 1, &shaderSource, NULL);
    glCompileShader(shaderId);
    
    GLint compilationStatus;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compilationStatus);
    
    if (!compilationStatus) {
        [self logCompilationError:shaderId];
        glDeleteShader(shaderId);
    }
    
    if (compilationStatus) {
        [self addToCache:shaderFilePathName value:shaderId];
    }
    
    return compilationStatus ? shaderId : 0;
}

+(void)logCompilationError:(GLuint)shaderId {
    GLint logLength;
    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(shaderId, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
}

+ (BOOL)linkProgram:(GLuint)programId {
    glLinkProgram(programId);
    GLint linkStatus;
    glGetProgramiv(programId, GL_LINK_STATUS, &linkStatus);
    return linkStatus ? YES : NO;
}

+ (void)extractUniformLocations:(NSArray *)uniformNames
                    fromProgram:(GLuint)programId
                      toStorage:(GLint *)storage {
    unsigned index = 0;
    for (NSString* uniformName in uniformNames) {
        storage[index++] = glGetUniformLocation(programId, [uniformName UTF8String]);
    }
}

+ (GLuint)loadShader:(NSString *)shaderResourceName
      WithAttributes:(NSDictionary *)attrs
  andExtractUniforms:(NSArray *)uniformNames
           toStorage:(GLint *)storage {
    GLuint progId = [EffectUtils loadShader:shaderResourceName
                             WithAttributes:attrs];
    [EffectUtils extractUniformLocations:uniformNames
                             fromProgram:progId toStorage:storage];
    return progId;
}

#pragma mark - Texture Utils

+ (OGLKTextureInfo *)textureWithCGImage:(CGImageRef)cgImage
                                options:(NSDictionary *)options {
    
    // Get the bytes to be used when copying data into new texture
    // buffer
    size_t width;
    size_t height;
    NSData *imageData = [self imageData:cgImage withResizedWidth:&width andHeight:&height];
    
    //glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    // Generation, bind, and copy data into a new texture buffer
    GLuint textureBufferID = 0;
    
    glGenTextures(1, &textureBufferID);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureBufferID);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
                 0, //border, should be 0 for OGL ES
                 GL_RGBA, GL_UNSIGNED_BYTE, [imageData bytes]);
    // Set parameters that control texture sampling for the bound texture
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    return [[OGLKTextureInfo alloc] initWithBufferId:textureBufferID type:GL_TEXTURE_2D
                                        width:width height:height];
}

+(NSData *)imageData:(CGImageRef)cgImage
    withResizedWidth:(size_t *)width
           andHeight:(size_t *)height {
    
    //fit to pow of 2 for performance
    *width = [self greatestPowerOfTwo:CGImageGetWidth(cgImage)];
    *height = [self greatestPowerOfTwo:CGImageGetWidth(cgImage)];
    
    
    NSMutableData *imageData = [NSMutableData dataWithLength:(*height)*(*width)*BYTES_PER_RGBA];
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef cgContext = CGBitmapContextCreate(
        [imageData mutableBytes], *width, *height, 8,
        BYTES_PER_RGBA * (*width), colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorSpace);
    
    // Flip the Core Graphics Y-axis for future drawing
    CGContextTranslateCTM (cgContext, 0, *height);
    CGContextScaleCTM (cgContext, 1.0, -1.0);
    
    // Draw the loaded image into the Core Graphics context
    CGContextDrawImage(cgContext, CGRectMake(0, 0, *width, *height), cgImage);
    CGContextRelease(cgContext);
    
    return imageData;
}

+(unsigned)greatestPowerOfTwo:(unsigned)value {
    --value;
    value |= value >> 1; value |= value >> 2;
    value |= value >> 4; value |= value >> 8;
    value |= value >> 16;
    return ++value;
}

@end
