//
//  ColoredOutput.vsh
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/16/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

attribute vec3 a_position;
attribute vec3 a_color;

varying mediump vec4 v_color;

uniform mat4 u_viewProjectionMatrix;
uniform float u_opacity;

void main() {
    v_color = vec4(a_color, u_opacity);
    gl_Position = u_viewProjectionMatrix * vec4(a_position, 1.0);
}
