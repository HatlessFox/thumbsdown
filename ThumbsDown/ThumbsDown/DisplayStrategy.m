//
//  DisplayStrategy.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/18/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "DisplayStrategy.h"

@implementation DisplayStrategy {
    NSString *_simObjName;
}

-(id)initWithSimObjectName:(NSString *)simObjName {
    self = [super init];
    if (self) {
        _simObjName = simObjName;
    }
    return self;
}

-(void)draw {
    //use identity as init matrix
    if (!self.mesh) { return; }
    [self.mesh draw:GLKMatrix4Identity];
}

@end
