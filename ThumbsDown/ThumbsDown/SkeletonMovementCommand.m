//
//  SkeletonMovementCommand.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/22/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "SkeletonMovementCommand.h"
#import "SimObjFactory.h"
#import "PhysicsManager.h"
#import "SkeletonPhysicsObject.h"

@implementation SkeletonMovementCommand {
    NSDictionary *_skeletonTranslation;
    NSString *_actorName;
}

-(id)initWithActorName:(NSString *)actorName
              commands:(NSDictionary *)skeletonTranslation {
    self = [super init];
    if (self) {
        _actorName = actorName;
        _skeletonTranslation = skeletonTranslation;
    }
    return self;
}

-(BOOL)performCommand {
    SimulationObject *obj = [SimObjFactory createdObjectById:_actorName];
    if (!obj) { return YES; }
    
    SkeletonPhysicsObject *spo = (SkeletonPhysicsObject *)obj.physicsSt.physObject;
    
    for (NSString *jointName in _skeletonTranslation) {
        Joint *joint = [spo jointById:jointName];
        [joint rotateChildOverJoint:[_skeletonTranslation[jointName] floatValue]];
    }
    
    [[PhysicsManager physicsObjectById:_actorName] prepareForCollisionDetection];
    BOOL canBeMoved = self.ignoreCollisionCheck ? TRUE :
        ![PhysicsManager checkCollisionsForObject:_actorName];
    if (!canBeMoved) {
        for (NSString *jointName in _skeletonTranslation) {
            Joint *joint = [spo jointById:jointName];
            [joint rotateChildOverJoint:(-[_skeletonTranslation[jointName] floatValue])];
        }
    }
    return canBeMoved;
}


@end
