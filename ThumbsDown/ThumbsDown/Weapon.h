//
//  Weapon.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/3/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkeletonPhysicsObject.h"
#import "GameStrategy.h"

extern NSString * const wpnPistolId;

@protocol Weapon

@required

-(float)fireRate;
-(NSArray *)createBulletWithId:(NSString *)bId;
-(void)assignToWarrior:(SkeletonPhysicsObject *)sltn;

-(void)postprocessShoot:(GLKVector3)shootDirection;
-(NSDictionary *)weaponRelatedAnimations;


@end
