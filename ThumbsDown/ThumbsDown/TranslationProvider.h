//
//  TranslationProvider.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/5/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GLKit;

@protocol TranspositionProvider <NSObject>

@required
-(GLKMatrix4)currentTransposition;

@end
