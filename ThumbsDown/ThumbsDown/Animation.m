//
//  Animation.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/23/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Animation.h"
#import "KeyFrame.h"

@implementation Animation {
    NSMutableArray *_keyFrames;
}

-(NSMutableArray *)keyFrames { return _keyFrames; }

-(id)createCopy {
    NSMutableArray *kfArr = [NSMutableArray new];
    for (id<KeyFrame> kf in _keyFrames) {
        [kfArr addObject:[kf createCopy]];
    }
    
    return [[Animation alloc] initWithKeyFrames:kfArr];
}


-(instancetype)initWithKeyFrames:(NSMutableArray *)keyFrames {
    self = [super init];
    if (self) {
        _keyFrames = [keyFrames mutableCopy];
    }
    return self;
}

-(void)setTargetId:(NSString *)targetId {
    _targetId = targetId;
    for (id<KeyFrame> kf in _keyFrames) {
        [kf setTargetId:targetId];
    }
}

-(BOOL)step:(float)elapsedTime {
    while (_keyFrames.count && [_keyFrames[0] step:elapsedTime]) {
        //TODO replace with stack
        [_keyFrames removeObjectAtIndex:0];
    }
    
    return _keyFrames.count == 0;
}

-(void)merge:(Animation *)animation {
    [_keyFrames addObjectsFromArray:[animation keyFrames]];
}


@end
